﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.VoltarDepartamento
{
   public class VoltarCargo
    {
        public int PesquisarID(CargoDTO dto)
        {
            string script =
                @"SELECT * FROM tb_cargo WHERE nm_cargo = @nm_cargo";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cargo", dto.Cargo));
            Space_cars_teste.DB.Base.Database db = new Space_cars_teste.DB.Base.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            int id = 0;
            while (reader.Read())
            {
                CargoDTO  dt = new CargoDTO();
                dt.ID_Cargo = reader.GetInt32("id_cargo");
                dt.Cargo  = reader.GetString("nm_cargo");
                id = dt.ID_Cargo;

            }
            return id;
        }
    }
}
