﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.VoltarDepartamento
{
   public class VoltarDepartamento
    {
        public int PesquisarID(DepartamentoDTO dto)
        {
            string script =
                @"SELECT * FROM tb_departamento WHERE nm_departamento = @nm_departamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_departamento", dto.Departamento));
            Space_cars_teste.DB.Base.Database db = new Space_cars_teste.DB.Base.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            int id = 0;
            while (reader.Read())
            {
                DepartamentoDTO dt = new DepartamentoDTO();
                dt.ID_Departamento = reader.GetInt32("id_departamento");
                dt.Departamento = reader.GetString("nm_departamento");
                id = dt.ID_Departamento;

            }
            return id;
        }
    }
}
