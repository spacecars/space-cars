﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.Ferramentas.Calcular_folha
{
    public class FolhadePagamento
    {
        public decimal CalcularFolha (decimal salario)
        {
            decimal total = 0;
            decimal HRextra = CalcValorHrExtra(salario);
            decimal DSR = CalcDSR(HRextra);
            decimal INSS = CalcINSS(salario, HRextra, DSR);
            decimal FGTS = CalcINSS(salario, HRextra, DSR);

            return total;
        }

        private decimal CalcValorHrExtra (decimal salario)
        {
            decimal ValorHr = salario / 220;
            decimal HrExtra = ValorHr / 2;
            return HrExtra;
        }

        private decimal CalcDSR (decimal Hrextra)
        {
            decimal DSR = (Hrextra / 26) * 4;
            return DSR;
        }
        
        private decimal CalcINSS (decimal salario, decimal Hrextra, decimal DSR)
        {
            decimal bruto = salario + Hrextra + DSR;
            decimal INSS = (bruto / 100) * 8;
            return INSS;
        }

        private decimal CalcFGTS (decimal salario, decimal Hrextra, decimal DSR)
        {
            decimal bruto = salario + Hrextra + DSR;
            decimal FGTS = (bruto / 100) * 8;
            return FGTS;
        }
    }
}
