﻿using Space_cars_teste.Forms.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Space_cars_teste.Forms.Login
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();


            Task.Factory.StartNew(() =>
            {

                System.Threading.Thread.Sleep(7500);

                Invoke(new Action(() =>
                {
                    frmSplash tela = new frmSplash();
                    tela.Show();
                    this.Hide();
                }));

            });
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
