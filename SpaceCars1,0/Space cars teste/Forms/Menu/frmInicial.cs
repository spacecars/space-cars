﻿using Space_cars_teste.User_Controls.Módulo_de_Acesso.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Space_cars_teste.Forms.Menu
{
    public partial class frmInicial : Form
    {
        public frmInicial()
        {
            InitializeComponent();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void menuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

        private void cadrastrarFuncionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarFuncionario tela = new frmCadastrarFuncionario();
            tela.Show();
            this.Hide();
        }

        private void consultarFuncionariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarFuncionario tela = new frmConsultarFuncionario();
            tela.Show();
            this.Hide();
        }

        private void adicionarrItensToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult b = MessageBox.Show("Deseja fechar o programa?",
                                            "SpaceCars",
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question);

            if(b ==  DialogResult.Yes)
            {
                Close();
            }

        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }
    }
}
