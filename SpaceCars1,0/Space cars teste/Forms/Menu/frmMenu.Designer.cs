﻿namespace Space_cars_teste.Forms.Menu
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.funcionariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadrastrarFuncionariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarFuncionariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.comprasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adicionarrItensToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadrastrarCompraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDeCompraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fornecedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadrastrarFornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarFornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produtosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadrastrarProdutosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarProdutosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel5 = new System.Windows.Forms.Panel();
            this.menuStrip5 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel6 = new System.Windows.Forms.Panel();
            this.menuStrip6 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel7 = new System.Windows.Forms.Panel();
            this.menuStrip7 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.menuStrip4.SuspendLayout();
            this.menuStrip3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.menuStrip5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.menuStrip6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.menuStrip7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Location = new System.Drawing.Point(1, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(141, 35);
            this.panel1.TabIndex = 8;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Black;
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip1.Font = new System.Drawing.Font("Perpetua Titling MT", 9.75F, System.Drawing.FontStyle.Bold);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.funcionariosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(139, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // funcionariosToolStripMenuItem
            // 
            this.funcionariosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadrastrarFuncionariosToolStripMenuItem,
            this.consultarFuncionariosToolStripMenuItem});
            this.funcionariosToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.funcionariosToolStripMenuItem.Name = "funcionariosToolStripMenuItem";
            this.funcionariosToolStripMenuItem.Size = new System.Drawing.Size(128, 29);
            this.funcionariosToolStripMenuItem.Text = "Funcionarios";
            // 
            // cadrastrarFuncionariosToolStripMenuItem
            // 
            this.cadrastrarFuncionariosToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cadrastrarFuncionariosToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.cadrastrarFuncionariosToolStripMenuItem.Name = "cadrastrarFuncionariosToolStripMenuItem";
            this.cadrastrarFuncionariosToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.cadrastrarFuncionariosToolStripMenuItem.Text = "Cadrastrar funcionarios";
            // 
            // consultarFuncionariosToolStripMenuItem
            // 
            this.consultarFuncionariosToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.consultarFuncionariosToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.consultarFuncionariosToolStripMenuItem.Name = "consultarFuncionariosToolStripMenuItem";
            this.consultarFuncionariosToolStripMenuItem.Size = new System.Drawing.Size(284, 22);
            this.consultarFuncionariosToolStripMenuItem.Text = "Consultar funcionarios";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.menuStrip2);
            this.panel2.Location = new System.Drawing.Point(1, 141);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(141, 35);
            this.panel2.TabIndex = 9;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.Color.Black;
            this.menuStrip2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip2.Font = new System.Drawing.Font("Perpetua Titling MT", 9.75F, System.Drawing.FontStyle.Bold);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(139, 33);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            this.menuStrip2.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip2_ItemClicked);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.comprasToolStripMenuItem,
            this.fornecedoresToolStripMenuItem,
            this.produtosToolStripMenuItem});
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.DarkGray;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(88, 29);
            this.toolStripMenuItem1.Text = "Compras";
            // 
            // comprasToolStripMenuItem
            // 
            this.comprasToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.comprasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adicionarrItensToolStripMenuItem,
            this.cadrastrarCompraToolStripMenuItem,
            this.consultaDeCompraToolStripMenuItem});
            this.comprasToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.comprasToolStripMenuItem.Name = "comprasToolStripMenuItem";
            this.comprasToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.comprasToolStripMenuItem.Text = "Compras";
            // 
            // adicionarrItensToolStripMenuItem
            // 
            this.adicionarrItensToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.adicionarrItensToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.adicionarrItensToolStripMenuItem.Name = "adicionarrItensToolStripMenuItem";
            this.adicionarrItensToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.adicionarrItensToolStripMenuItem.Text = "Adicionar itens";
            // 
            // cadrastrarCompraToolStripMenuItem
            // 
            this.cadrastrarCompraToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cadrastrarCompraToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.cadrastrarCompraToolStripMenuItem.Name = "cadrastrarCompraToolStripMenuItem";
            this.cadrastrarCompraToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.cadrastrarCompraToolStripMenuItem.Text = "Cadastrar compra";
            // 
            // consultaDeCompraToolStripMenuItem
            // 
            this.consultaDeCompraToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.consultaDeCompraToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.consultaDeCompraToolStripMenuItem.Name = "consultaDeCompraToolStripMenuItem";
            this.consultaDeCompraToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.consultaDeCompraToolStripMenuItem.Text = "Consulta de Compra";
            // 
            // fornecedoresToolStripMenuItem
            // 
            this.fornecedoresToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.fornecedoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadrastrarFornecedorToolStripMenuItem,
            this.consultarFornecedorToolStripMenuItem});
            this.fornecedoresToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.fornecedoresToolStripMenuItem.Name = "fornecedoresToolStripMenuItem";
            this.fornecedoresToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.fornecedoresToolStripMenuItem.Text = "Fornecedores";
            // 
            // cadrastrarFornecedorToolStripMenuItem
            // 
            this.cadrastrarFornecedorToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cadrastrarFornecedorToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.cadrastrarFornecedorToolStripMenuItem.Name = "cadrastrarFornecedorToolStripMenuItem";
            this.cadrastrarFornecedorToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.cadrastrarFornecedorToolStripMenuItem.Text = "Cadastrar fornecedor";
            // 
            // consultarFornecedorToolStripMenuItem
            // 
            this.consultarFornecedorToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.consultarFornecedorToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.consultarFornecedorToolStripMenuItem.Name = "consultarFornecedorToolStripMenuItem";
            this.consultarFornecedorToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.consultarFornecedorToolStripMenuItem.Text = "Consultar Fornecedor";
            // 
            // produtosToolStripMenuItem
            // 
            this.produtosToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.produtosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadrastrarProdutosToolStripMenuItem,
            this.consultarProdutosToolStripMenuItem});
            this.produtosToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.produtosToolStripMenuItem.Name = "produtosToolStripMenuItem";
            this.produtosToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.produtosToolStripMenuItem.Text = "Produtos";
            // 
            // cadrastrarProdutosToolStripMenuItem
            // 
            this.cadrastrarProdutosToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cadrastrarProdutosToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.cadrastrarProdutosToolStripMenuItem.Name = "cadrastrarProdutosToolStripMenuItem";
            this.cadrastrarProdutosToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.cadrastrarProdutosToolStripMenuItem.Text = "Cadastrar produtos";
            // 
            // consultarProdutosToolStripMenuItem
            // 
            this.consultarProdutosToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.consultarProdutosToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGray;
            this.consultarProdutosToolStripMenuItem.Name = "consultarProdutosToolStripMenuItem";
            this.consultarProdutosToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.consultarProdutosToolStripMenuItem.Text = "Consultar produtos";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.menuStrip3);
            this.panel3.Location = new System.Drawing.Point(1, 264);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(141, 35);
            this.panel3.TabIndex = 10;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.menuStrip4);
            this.panel4.Location = new System.Drawing.Point(-1, -1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(141, 35);
            this.panel4.TabIndex = 9;
            // 
            // menuStrip4
            // 
            this.menuStrip4.BackColor = System.Drawing.Color.Black;
            this.menuStrip4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip4.Font = new System.Drawing.Font("Perpetua Titling MT", 9.75F, System.Drawing.FontStyle.Bold);
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3});
            this.menuStrip4.Location = new System.Drawing.Point(0, 0);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.Size = new System.Drawing.Size(139, 33);
            this.menuStrip4.TabIndex = 0;
            this.menuStrip4.Text = "menuStrip4";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.ForeColor = System.Drawing.Color.DarkGray;
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(41, 29);
            this.toolStripMenuItem3.Text = "RH";
            // 
            // menuStrip3
            // 
            this.menuStrip3.BackColor = System.Drawing.Color.Black;
            this.menuStrip3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip3.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip3.Font = new System.Drawing.Font("Perpetua Titling MT", 9.75F, System.Drawing.FontStyle.Bold);
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.menuStrip3.Location = new System.Drawing.Point(0, 0);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Size = new System.Drawing.Size(134, 33);
            this.menuStrip3.TabIndex = 0;
            this.menuStrip3.Text = "menuStrip3";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.ForeColor = System.Drawing.Color.DarkGray;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(121, 20);
            this.toolStripMenuItem2.Text = "Funcionarios";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.menuStrip5);
            this.panel5.Location = new System.Drawing.Point(1, 223);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(141, 35);
            this.panel5.TabIndex = 9;
            // 
            // menuStrip5
            // 
            this.menuStrip5.BackColor = System.Drawing.Color.Black;
            this.menuStrip5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip5.Font = new System.Drawing.Font("Perpetua Titling MT", 9.75F, System.Drawing.FontStyle.Bold);
            this.menuStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4});
            this.menuStrip5.Location = new System.Drawing.Point(0, 0);
            this.menuStrip5.Name = "menuStrip5";
            this.menuStrip5.Size = new System.Drawing.Size(139, 33);
            this.menuStrip5.TabIndex = 0;
            this.menuStrip5.Text = "menuStrip5";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.ForeColor = System.Drawing.Color.DarkGray;
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(96, 29);
            this.toolStripMenuItem4.Text = "Finaceiro";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.menuStrip6);
            this.panel6.Location = new System.Drawing.Point(1, 182);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(141, 35);
            this.panel6.TabIndex = 9;
            // 
            // menuStrip6
            // 
            this.menuStrip6.BackColor = System.Drawing.Color.Black;
            this.menuStrip6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip6.Font = new System.Drawing.Font("Perpetua Titling MT", 9.75F, System.Drawing.FontStyle.Bold);
            this.menuStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5});
            this.menuStrip6.Location = new System.Drawing.Point(0, 0);
            this.menuStrip6.Name = "menuStrip6";
            this.menuStrip6.Size = new System.Drawing.Size(139, 33);
            this.menuStrip6.TabIndex = 0;
            this.menuStrip6.Text = "menuStrip6";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.ForeColor = System.Drawing.Color.DarkGray;
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(85, 29);
            this.toolStripMenuItem5.Text = "Estoque";
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.menuStrip7);
            this.panel7.Location = new System.Drawing.Point(1, 305);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(141, 35);
            this.panel7.TabIndex = 11;
            // 
            // menuStrip7
            // 
            this.menuStrip7.BackColor = System.Drawing.Color.Black;
            this.menuStrip7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip7.Font = new System.Drawing.Font("Perpetua Titling MT", 9.75F, System.Drawing.FontStyle.Bold);
            this.menuStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem6});
            this.menuStrip7.Location = new System.Drawing.Point(0, 0);
            this.menuStrip7.Name = "menuStrip7";
            this.menuStrip7.Size = new System.Drawing.Size(139, 33);
            this.menuStrip7.TabIndex = 0;
            this.menuStrip7.Text = "menuStrip7";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.ForeColor = System.Drawing.Color.DarkGray;
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(77, 29);
            this.toolStripMenuItem6.Text = "Vendas";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Space_cars_teste.Properties.Resources.Botao_fechar_caseiro;
            this.pictureBox1.Location = new System.Drawing.Point(653, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 31);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::Space_cars_teste.Properties.Resources.tela_de_menu;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(700, 410);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMenu";
            this.Text = "frmMenu";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.menuStrip5.ResumeLayout(false);
            this.menuStrip5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.menuStrip6.ResumeLayout(false);
            this.menuStrip6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.menuStrip7.ResumeLayout(false);
            this.menuStrip7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem funcionariosToolStripMenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem comprasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adicionarrItensToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadrastrarCompraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaDeCompraToolStripMenuItem;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.MenuStrip menuStrip5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.MenuStrip menuStrip6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.MenuStrip menuStrip7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem cadrastrarFuncionariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarFuncionariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fornecedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadrastrarFornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarFornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadrastrarProdutosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarProdutosToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}