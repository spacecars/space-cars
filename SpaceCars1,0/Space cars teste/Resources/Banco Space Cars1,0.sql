
drop database Restaurante;


CREATE DATABASE SpaceCars;
USE `SpaceCars` ;


-- -----------------------------------------------------
-- Table `Restaurante`.`tb_funcionario`
-- -----------------------------------------------------
CREATE TABLE `SpaceCars`.`tb_funcionario` (
  `id_funcionario` INT NOT NULL AUTO_INCREMENT,
  `nm_funcionario` VARCHAR(55) NOT NULL,
  `ds_rg` VARCHAR(15) NOT NULL,
  `ds_cpf` VARCHAR(20) NOT NULL unique,
  `dt_nascimento` DATE NOT NULL,
  `tp_sexo` VARCHAR(10) NOT NULL,
  `vl_vale_transporte` DECIMAL(7,2) NULL,
  `vl_vale_refeicao` DECIMAL(7,2) NULL,
  `vl_vale_alimentacao` DECIMAL(7,2) NULL,
  `vl_vale_convenio` DECIMAL(7,2) NULL,
  `vl_salario` DECIMAL(7,2) NOT NULL,
  `img_foto` LONGBLOB NULL,
  `ds_usuario` VARCHAR(60) NOT NULL unique,
  `ds_senha` VARCHAR(15) NOT NULL,
  `nm_uf` VARCHAR(10) NOT NULL,
  `nm_cidade` VARCHAR(75) NOT NULL,
  `nm_endereco` VARCHAR(115) NOT NULL,
  `nr_residencia` INT NOT NULL,
  `ds_cep` VARCHAR(10) NULL,
  `ds_complemento` VARCHAR(10) NULL,
  `ds_email` VARCHAR(60) NULL,
  `ds_telefone` VARCHAR(20) NULL,
  `ds_celular` VARCHAR(20) NULL,
  `id_cargo` INT NOT NULL,
  PRIMARY KEY (`id_funcionario`)
  
  CONSTRAINT contraint_funcionario_fk0 FOREIGN KEY (`id_cargo`) 
  REFERENCES `SpaceCars`.`tb_cargo` (`id_cargo`)
  );

  
INSERT INTO `SpaceCars`.`tb_funcionario` (`id_funcionario`, `nm_funcionario`, `ds_RG`, `ds_CPF`, `dt_nascimento`, `tp_sexo`, `vl_vale_transporte`, `vl_vale_refeicao`, `vl_vale_alimentacao`, `vl_vale_convenio`, `vl_salario`, `ds_usuario`, `ds_senha`, `nm_uf`, `nm_cidade`, `nm_endereco`, `nr_residencia`, `ds_CEP`, `ds_complemento`, `ds_email`, `ds_telefone`, `ds_celular`) 
VALUES ('1', 'Marcos', '39.100.460-8', '480.078.808-01', '1999/02/18', '1', '1000.00', '1000.00', '1000.00', '1000.00', '1000.00', 'adm', '123', 'SP', 'São Paulo', 'Vila do Sol', '17', '04962-000', 'B', 'philippedph96@gmail.com', '(11)5517-4300', '(11)99606-2701');
 
INSERT INTO `SpaceCars`.`tb_funcionario` (`id_funcionario`, `nm_funcionario`, `ds_RG`, `ds_CPF`, `dt_nascimento`, `tp_sexo`, `vl_vale_transporte`, `vl_vale_refeicao`, `vl_vale_alimentacao`, `vl_vale_convenio`, `vl_salario`, `ds_usuario`, `ds_senha`, `nm_uf`, `nm_cidade`, `nm_endereco`, `nr_residencia`, `ds_CEP`, `ds_complemento`, `ds_email`, `ds_telefone`, `ds_celular`) 
VALUES ('2', 'Davi', '11.111.111-1', '111.111.111-11', '1997/08/12', '1', '1000.00', '1000.00', '1000.00', '1000.00', '1000.00', 'davi', '123', 'SP', 'São Paulo', 'Vila do Sol', '17', '04962-000', 'B', 'davig_unit@hotmail.com', '(11)5517-4300', '(11)99606-2701');



-- -----------------------------------------------------
-- Table `Restaurante`.`tb_departamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SpaceCars`.`tb_cargo` (
  `id_cargo` INT NOT NULL AUTO_INCREMENT,
  `nm_cargo` VARCHAR(50) NOT NULL,
  `ds_salario` DECIMAL(7,2) NOT NULL,
  `ds_horaextra` DECIMAL(7,2) NOT NULL,
  PRIMARY KEY (`id_cargo`));

INSERT INTO tb_departamento(`nm_cargo`, `ds_salario`, 'ds_horaextra') 
Values 
('Gerente Geral', '2058.00', '14.04'),('Auxiliar Administrativo', '1325.00', '9.03'),
('Lavadora de Carros', '1257.00', '8.56'), ('Operadora de Caixa', '1762.00', '12.00'),
('Faxineira', '954.00', '6.49'), ('Mecanico', '1100.00', '7.50');

-- -----------------------------------------------------
-- Table `Restaurante`.`tb_folha_pagamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SpaceCars`.`tb_folha_pagamento` (
  `id_folha_pagamento` INT NOT NULL AUTO_INCREMENT,
  `nr_horas_trabalhadas` INT NULL,
  `nr_horas_extra_trabalhadas` INT NULL,
  `vl_horas_extras` DECIMAL(7,2) NULL,
  `vl_fgts` DECIMAL(7,2) NULL,
  `vl_inss` DECIMAL(7,2) NULL,
  `vl_dsr` DECIMAL(7,2) NULL,
  `vl_bruto` DECIMAL(7,2) NOT NULL,
  `vl_liquido` DECIMAL(7,2) NOT NULL,
  `id_funcionario` INT NOT NULL,
  PRIMARY KEY (`id_folha_pagamento`),
  
  CONSTRAINT contraint_folha_fk0 FOREIGN KEY (`id_funcionario`) 
  REFERENCES `SpaceCars`.`tb_funcionario` (`id_funcionario`)
  );


-- -----------------------------------------------------
-- Table `Restaurante`.`tb_ponto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SpaceCars`.`tb_ponto` (
  `id_ponto` INT NOT NULL AUTO_INCREMENT,
  `dt_data` DATE NOT NULL,
  `hr_entrada` VARCHAR(5) NOT NULL,
  `hr_almoco_saida` VARCHAR(5) NULL,
  `hr_almoco_retorno` VARCHAR(5) NULL,
  `hr_saida` VARCHAR(5) NOT NULL,
  `id_funcionario` INT NOT NULL,
  PRIMARY KEY (`id_ponto`),
  
  CONSTRAINT contraint_ponto_fk0 FOREIGN KEY (`id_funcionario`) REFERENCES `SpaceCars`.`tb_funcionario` (`id_funcionario`)
   );


-- -----------------------------------------------------
-- Table `Restaurante`.`tb_cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SpaceCars`.`tb_cliente` (  -- Okay
  `id_cliente` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `ds_cpf` VARCHAR(25) NULL,
  `ds_rg` VARCHAR (20) NULL,
  `nm_nome` VARCHAR(50) NULL,
  `dt_data_nascimento` DATE NULL,
  `ds_cep` VARCHAR(20) NULL,
  `ds_uf` VARCHAR(10) NULL,
  `ds_endereco` VARCHAR(70) NULL,
  `ds_telefone` VARCHAR(20) NULL,
  `ds_celular` VARCHAR(20) NULL,
  `ds_tipo_de_carta` CHAR(3) NULL,
  `ds_email` VARCHAR(70) NULL,
  `ds_sexo` BOOL NULL,
  `ds_complemento` VARCHAR (20) NULL,
  `nr_numero` INT NULL,
  PRIMARY KEY(`id_cliente`));


-- -----------------------------------------------------
-- Table `Restaurante`.`tb_servicos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SpaceCars`.`tb_servicos` (
  `id_servicos` INT NOT NULL AUTO_INCREMENT,
  `nm_servicos` VARCHAR(55) NOT NULL,
  `ds_servicos` VARCHAR(55) NOT NULL,
  `vl_preco` DECIMAL(5,2) NOT NULL,
  `img_foto` LONGBLOB NULL,
  PRIMARY KEY (`id_servicos`));


-- -----------------------------------------------------
-- Table `Restaurante`.`tb_pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SpaceCars`.`tb_pedido` (
  `id_pedido` INT NOT NULL AUTO_INCREMENT,
  `id_cliente` INT NOT NULL,
  `id_servicos` INT NOT NULL,
  `dt_venda` DATE NOT NULL,
  `id_funcionario` INT NOT NULL,
  PRIMARY KEY (`id_pedido`),
  CONSTRAINT contraint_pedido_fk0 FOREIGN KEY (`id_cliente`) REFERENCES `SpaceCars`.`tb_cliente` (`id_cliente`),
  CONSTRAINT contraint_folha_fk1 FOREIGN KEY (`id_servicos`) REFERENCES `SpaceCars`.`tb_servicos` (`id_servicos`),
  CONSTRAINT contraint_folha_fk2 FOREIGN KEY (`id_funcionario`) REFERENCES `SpaceCars`.`tb_funcionario` (`id_funcionario`));


-- -----------------------------------------------------
-- Table `Restaurante`.`tb_produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SpaceCars`.`tb_produto` (
  `id_produto` INT NOT NULL AUTO_INCREMENT,
  `nm_produto` VARCHAR(55) NOT NULL,
  `vl_preco` DECIMAL(5,2) NOT NULL,
  `nm_marca` VARCHAR(55) NULL,
  PRIMARY KEY (`id_produto`));


-- -----------------------------------------------------
-- Table `Restaurante`.`tb_fornecedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SpaceCars`.`tb_fornecedor` (
  `id_fornecedor` INT NOT NULL AUTO_INCREMENT,
  `ds_razao_social` VARCHAR(150) NOT NULL,
  `ds_cnpj` VARCHAR(30) NOT NULL,
  `ds_telefone` VARCHAR(15) NULL,
  `ds_email` VARCHAR(55) NULL,
  `ds_endereco` VARCHAR (70) NULL,
  `ds_cep` VARCHAR (10) NULL,
  `ds_complemento` VARCHAR (10) NULL,
  `nr_casa` SMALLINT NULL,
  `ds_uf` VARCHAR (10) NULL,
  `ds_cidade` VARCHAR (25) NULL,
  `ds_observacao` VARCHAR (200) NULL,

  PRIMARY KEY (`id_fornecedor`));


-- -----------------------------------------------------
-- Table `Restaurante`.`tb_compra_fornecedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SpaceCars`.`tb_compra_fornecedor` (
  `id_compra` INT NOT NULL AUTO_INCREMENT,
  `id_fornecedor` INT NOT NULL,
  `id_produto` INT NOT NULL,
  `ds_quantidade` INT NOT NULL,
  `id_funcionario` INT NOT NULL,
  `dt_compra` DATE NOT NULL,
  `ds_lote` VARCHAR(30) NOT NULL,
  `dt_validade` DATE NOT NULL,
  `dt_compra` DATE NOT NULL,
  PRIMARY KEY (`id_compra`),
  CONSTRAINT contraint_compra_fk0 FOREIGN KEY (`id_fornecedor`) REFERENCES `SpaceCars`.`tb_fornecedor` (`id_fornecedor`),
  CONSTRAINT contraint_compra_fk1  FOREIGN KEY (`id_produto`) REFERENCES `SpaceCars`.`tb_produto` (`id_produto`),
  CONSTRAINT contraint_compra_fk2  FOREIGN KEY (`id_funcionario`) REFERENCES `SpaceCars`.`tb_funcionario` (`id_funcionario`)
    );


-- -----------------------------------------------------
-- Table `Restaurante`.`tb_estoque`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SpaceCars`.`tb_estoque` (
  `id_estoque` INT NOT NULL AUTO_INCREMENT,
  `qtd_minima` VARCHAR(20) NOT NULL,
  `qtd_maxima` VARCHAR(20) NOT NULL,
  `qtd_atual` VARCHAR(20) NOT NULL,
  `id_produto` INT NOT NULL,
  PRIMARY KEY (`id_estoque`),
  CONSTRAINT contraint_estoque_fk0  FOREIGN KEY (`id_produto`) REFERENCES `SpaceCars`.`tb_produto` (`id_produto`));


-- -----------------------------------------------------
-- Table `Restaurante`.`tb_nivel_acesso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SpaceCars`.`tb_nivel_acesso` (
  `id_nivel_acesso` INT NOT NULL AUTO_INCREMENT,
  `id_funcionario` INT NOT NULL,
  `id_cargo` INT NOT NULL,
  `bl_salvar` bool not NULL,
  `bl_remover` bool not NULL,
  `bl_alterar` bool not NULL,
  `bl_consultar` bool not NULL,
  PRIMARY KEY (`id_nivel_acesso`),
  CONSTRAINT contraint_acesso_fk0  FOREIGN KEY (`id_funcionario`) REFERENCES `SpaceCars`.`tb_funcionario` (`id_funcionario`),
  CONSTRAINT contraint_acesso_fk1  FOREIGN KEY (`id_cargo`) REFERENCES `SpaceCars`.`tb_cargo` (`id_cargo`)
);
INSERT INTO `SpaceCars`.`tb_nivel_acesso` (`id_nivel_acesso`, `id_funcionario`,
            `id_cargo`, `bl_salvar`, `bl_remover`, `bl_alterar`, `bl_consultar`)
			  VALUES ('1', '1', '1', '1', '1', '1', '1');

INSERT INTO `SpaceCars`.`tb_nivel_acesso` (`id_nivel_acesso`, `id_funcionario`,
            `id_cargo`, `bl_salvar`, `bl_remover`, `bl_alterar`, `bl_consultar`)
			 VALUES ('2', '2', '1', '1', '0', '0', '0');

SELECT * FROM tb_nivel_acesso;
    
    -- VIEW FOLHA DE PAGAMENTO
    
CREATE VIEW view_folha_pagamento AS
select tb_folha_pagamento.id_folha_pagamento, tb_folha_pagamento.nr_horas_trabalhadas,
tb_folha_pagamento.vl_horas_extras, tb_folha_pagamento.vl_FGTS, tb_folha_pagamento.vl_INSS,
tb_folha_pagamento.vl_IR, tb_folha_pagamento.vl_dependente, tb_folha_pagamento.vl_bruto,
tb_folha_pagamento.vl_liquido, tb_funcionario.id_funcionario, tb_funcionario.nm_funcionario,
tb_funcionario.vl_salario
from tb_folha_pagamento
join tb_funcionario
 on tb_folha_pagamento.id_funcionario = tb_funcionario.id_funcionario;
 
CREATE VIEW view_acesso_funcionario AS
 SELECT tb_funcionario.id_funcionario,
		tb_cargo.id_cargo,
        tb_nivel_acesso.id_nivel_acesso,
        tb_nivel_acesso.bl_salvar,
        tb_nivel_acesso.bl_remover,
        tb_nivel_acesso.bl_alterar,
        tb_nivel_acesso.bl_consultar,
        tb_funcionario.ds_senha,
        tb_funcionario.ds_usuario
        
FROM tb_nivel_acesso JOIN tb_funcionario
  ON tb_nivel_acesso.id_funcionario = tb_funcionario.id_funcionario
JOIN tb_cargo
  ON tb_cargo.id_cargo= tb_nivel_acesso.id_cargo;