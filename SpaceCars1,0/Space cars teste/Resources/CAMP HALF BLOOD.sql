SELECT * FROM tb_chale;
SELECT * FROM tb_cronograma;
SELECT * FROM tb_forja;
SELECT * FROM tb_local;
SELECT * FROM tb_meiosangue;
SELECT * FROM tb_mitos;
SELECT * FROM tb_atividade;
SELECT * FROM tb_eventos;
SELECT * FROM tb_chale;
SELECT * FROM tb_ferreiro;
SELECT * FROM tb_senha;

-- -----------------------------------------------------
-- Schema Half Camp Blood
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Camp Half Blood` DEFAULT CHARACTER SET utf8 ;

CREATE DATABASE `Camp Half Blood`;
USE `Camp Half Blood` ;

-- -----------------------------------------------------
-- Table `Camp Half Blood`.`tb_chale`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Camp Half Blood`.`tb_chale` (
  `id_chale` INT NOT NULL AUTO_INCREMENT,
  `nm_nome` VARCHAR(45) NOT NULL,
  `nm_representante` VARCHAR(45),
  `ds_descricao` VARCHAR(70) NOT NULL,
  `img_simbolo` LONGBLOB,
  `tb_cronograma_id_cronograma` INT,
  PRIMARY KEY (`id_chale`),
  INDEX `fk_tb_chale_tb_cronograma1_idx` (`id_cronograma` ASC),
  CONSTRAINT `fk_tb_chale_tb_cronograma1`
    FOREIGN KEY (`id_cronograma`)
    REFERENCES `Camp Half Blood`.`tb_cronograma` (`id_cronograma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Camp Half Blood`.`tb_cronograma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Camp Half Blood`.`tb_cronograma` (
  `id_cronograma` INT NOT NULL AUTO_INCREMENT,
  `dt_diasemana` VARCHAR(15) NOT NULL,
  `dt_hora` VARCHAR(45) NOT NULL,
  `id_chale` INT NOT NULL,
  `id_tividade` INT NOT NULL,
  PRIMARY KEY (`id_cronograma`),
  INDEX `fk_tb_cronograma_tb_tividade1_idx` (`id_tividade` ASC),
  CONSTRAINT `fk_tb_cronograma_tb_tividade1`
    FOREIGN KEY (`id_tividade`)
    REFERENCES `Camp Half Blood`.`tb_tividade` (`id_tividade`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Camp Half Blood`.`tb_deus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Camp Half Blood`.`tb_deus` (
  `id_deus` INT NOT NULL AUTO_INCREMENT,
  `nm_deus` VARCHAR(45) NOT NULL,
  `img_foto` VARCHAR(45) NOT NULL,
  `img_simbolo` VARCHAR(45) NOT NULL,
  `ds_armaprincipal` VARCHAR(45) NOT NULL,
  `ds_habilidades` VARCHAR(45) NOT NULL,
  `ds_descricao` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id_deus`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `Camp Half Blood`.`tb_senha`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Camp Half Blood`.`tb_senha` (
  `id_senha` INT NOT NULL AUTO_INCREMENT,
  `nm_deus` VARCHAR(10) NOT NULL,
  `ds_senha` VARCHAR(10) NOT NULL,
  `img_backgroud` LONGBLOB NOT NULL,
  PRIMARY KEY (`id_senha`)
  );

-- -----------------------------------------------------
-- Table `Camp Half Blood`.`tb_forja`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Camp Half Blood`.`tb_forja` (
  `id_forja` INT NOT NULL AUTO_INCREMENT,
  `ds_pedido` VARCHAR(45) NOT NULL,
  `nm_ferreiro` VARCHAR(45) NOT NULL,
  `id_meiosangue` INT,
  `id_ferreiro` INT,
  PRIMARY KEY (`id_forja`),
  INDEX `fk_tb_forja_tb_meiosangue1_idx` (`id_meiosangue` ASC),
  INDEX `fk_tb_forja_tb_ferreiro_idx` (`id_ferreiro` ASC),
  CONSTRAINT `fk_tb_forja_tb_meiosangue1`
    FOREIGN KEY (`id_meiosangue`)
    REFERENCES `Camp Half Blood`.`tb_meiosangue` (`id_meiosangue`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_forja_tb_ferreiro`
    FOREIGN KEY (`id_ferreiro`)
    REFERENCES `Camp Half Blood`.`tb_ferreiro` (`id_ferreiro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Camp Half Blood`.`tb_ferreiro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Camp Half Blood`.`tb_ferreio`(
	`id_ferreiro` INT NOT NULL AUTO_INCREMENT,
    `dt_inicio` DATETIME NOT NULL,
    `id_semideus` INT NOT NULL,
    PRIMARY KEY (`id_ferreiro`)
    );


-- -----------------------------------------------------
-- Table `Camp Half Blood`.`tb_local`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Camp Half Blood`.`tb_local` (
  `id_local` INT NOT NULL AUTO_INCREMENT,
  `nm_local` VARCHAR(45) NOT NULL,
  `ds_local` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_local`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Camp Half Blood`.`tb_meiosangue`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Camp Half Blood`.`tb_meiosangue` (
  `id_meiosangue` INT NOT NULL AUTO_INCREMENT,
  `nm_meiosangue` VARCHAR(45) NOT NULL,
  `img_foto` LONGBLOB NOT NULL,
  `dt_datanascimento` VARCHAR(45) NOT NULL,
  `dt_dataentrada` VARCHAR(45) NOT NULL,
  `ds_armaprimaria` VARCHAR(45) NOT NULL,
  `ds_armasecundaria` VARCHAR(45) NOT NULL,
  `ds_habilidades` VARCHAR(45) NOT NULL,
  `nm_usuario` VARCHAR(45) NOT NULL,
  `ds_senha` VARCHAR(10) NOT NULL,
  `id_chale` INT NOT NULL,
  `id_deus` INT NOT NULL,
  PRIMARY KEY (`id_meiosangue`),
  INDEX `fk_tb_meiosangue_tb_chale_idx` (`id_chale` ASC),
  INDEX `fk_tb_meiosangue_tb_deus1_idx` (`id_deus` ASC),
  CONSTRAINT `fk_tb_meiosangue_tb_chale`
    FOREIGN KEY (`id_chale`)
    REFERENCES `Camp Half Blood`.`tb_chale` (`id_chale`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_meiosangue_tb_deus1`
    FOREIGN KEY (`id_deus`)
    REFERENCES `Camp Half Blood`.`tb_deus` (`id_deus`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Camp Half Blood`.`tb_mitos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Camp Half Blood`.`tb_mitos` (
  `id_mitos` INT NOT NULL AUTO_INCREMENT,
  `nm_mito` VARCHAR(45) NOT NULL,
  `ds_foto` LONGBLOB NOT NULL,
  `ds_descricao` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`id_mitos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Camp Half Blood`.`tb_tividade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Camp Half Blood`.`tb_tividade` (
  `id_tividade` INT NOT NULL AUTO_INCREMENT,
  `nm_atividade` VARCHAR(25) NOT NULL,
  `ds_descricao` VARCHAR(50) NOT NULL,
  `ds_equipamento` VARCHAR(25) NOT NULL,
  `nm_lecionador` VARCHAR(45) NOT NULL,
  `id_local` INT NOT NULL,
  PRIMARY KEY (`id_tividade`),
  INDEX `fk_tb_tividade_tb_local1_idx` (`id_local` ASC),
  CONSTRAINT `fk_tb_tividade_tb_local1`
    FOREIGN KEY (`id_local`)
    REFERENCES `Camp Half Blood`.`tb_local` (`id_local`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Half Camp Blood`.`tb_ventos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Camp Half Blood`.`tb_eventos` (
  `id_eventos` INT NOT NULL AUTO_INCREMENT,
  `nm_evento` VARCHAR(45) NOT NULL,
  `ds_descricao` VARCHAR(60) NOT NULL,
  `ds_primeiro` VARCHAR(45) NOT NULL,
  `ds_segundo` VARCHAR(45) NOT NULL,
  `ds_terceiro` VARCHAR(45),
  `dt_data` DATE NOT NULL,
  `dt_hora` DATE NOT NULL,
  PRIMARY KEY (`id_eventos`))
ENGINE = InnoDB;
