﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Space_cars_teste.nsfSpace_Car_Database.Módulo_de_Compra;
using Space_cars_teste.nsfspace_Car_DTO_s.Módulo_de_Compra;

namespace Space_cars_teste.nsfSapce_Car_Business.Módulo_de_Compras
{
    public class Business_Compra
    {
       
          //Método de escopo
          Database_Compra db = new Database_Compra();

         //Método de Salvar. Tudo começa pelo PARÂMETROS
          public int Salvar(DTO_Compra compra, List<DTO_Produto> produto)
          {
                //Pego o ID
                int idCompra = db.Salvar(compra);

                //Chamo a Business que contém o ID de produto e compra(Pedido)
                Business_CompraItem itembusiness = new Business_CompraItem();

                //Para cada produto encontrado
                foreach (DTO_Produto item in produto)
                {
                    //Instacio
                    DTO_CompraItem itemdto = new DTO_CompraItem();

                    //Passo os ID
                    itemdto.ID_Compra = idCompra;
                    itemdto.ID_Produto = item.ID;

                    //E salvo, primeiramente, na tb_compra_item
                    itembusiness.Salvar(itemdto);
                }
                //Agora, eu passo o valor encontrado para o Database
                return idCompra;
          }

        internal void Salvar(DTO_Compra dto, object p)
        {
            throw new NotImplementedException();
        }
    }
}
