﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.nsfspace_Car_DTO_s.Módulo_de_Compra
{
    public class VIEW_Compra
    {
        public class VIEW_CompraDTO
        {
            public int ID { get; set; }
            public DateTime Data { get; set; }
            public string Fornecedor { get; set; }
            public int Quantidade { get; set; }
            public decimal Total { get; set; }
        }
    }
}
