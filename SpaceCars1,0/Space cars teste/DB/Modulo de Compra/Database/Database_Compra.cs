﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.nsfSpace_Car_Database.Módulo_de_Compra
{
    public class Database_Compra
    {
        public int Salvar(nsfspace_Car_DTO_s.Módulo_de_Compra.DTO_Compra dto)
        {
            string script =
            @"INSERT INTO tb_compra_fornecedor
            (
                id_fornecedor,
                dt_compra
            )
            VALUES
            (
                @id_fornecedor,
                @dt_compra
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_fornecedor", dto.ID_Fornecedor));
            parm.Add(new MySqlParameter("dt_compra", dto.Data_Compra));

            DB.Base.Database db = new DB.Base.Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }
    }
}
