﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.nsfSpace_Car_Database.Módulo_de_Compra
{
    class MySqlParameter
    {
        private string v;
        private DateTime comeco;
        private int iD_Fornecedor;
        private string cNPJ;
        private decimal numero_Casa;

        public MySqlParameter(string v, DateTime comeco)
        {
            this.v = v;
            this.comeco = comeco;
        }

        public MySqlParameter(string v, int iD_Fornecedor)
        {
            this.v = v;
            this.iD_Fornecedor = iD_Fornecedor;
        }

        public MySqlParameter(string v, string cNPJ)
        {
            this.v = v;
            this.cNPJ = cNPJ;
        }

        public MySqlParameter(string v, decimal numero_Casa)
        {
            this.v = v;
            this.numero_Casa = numero_Casa;
        }

        internal class MySqlParameter
        {
            private string v;
            private DateTime data_Compra;
            private int iD_Fornecedor;

            public MySqlParameter(string v, DateTime data_Compra)
            {
                this.v = v;
                this.data_Compra = data_Compra;
            }

            public MySqlParameter(string v, int iD_Fornecedor)
            {
                this.v = v;
                this.iD_Fornecedor = iD_Fornecedor;
            }
        }
    }
}
