﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Space_cars_teste.DB.Base;
using static Space_cars_teste.nsfspace_Car_DTO_s.Módulo_de_Compra.VIEW_Compra;

namespace Space_cars_teste.nsfSpace_Car_Database.Módulo_de_Compra
{
    public class VIEW_Compra
    {
        public List<VIEW_CompraDTO> Consultar(DateTime comeco, DateTime fim)
        {
            string script = @"SELECT * FROM view_consultar_pedidos
                                      WHERE dt_compra >= @comeco
                                        AND dt_compra <= @fim";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("comeco", comeco));
            parm.Add(new MySqlParameter("fim", fim));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<VIEW_CompraDTO> fora = new List<VIEW_CompraDTO>();

            while (reader.Read())
            {
                VIEW_CompraDTO dentro = new VIEW_CompraDTO();
                dentro.ID = reader.GetInt32("id_compra");
                dentro.Data = reader.GetDateTime("dt_compra");
                dentro.Fornecedor = reader.GetString("ds_razao_social");
                dentro.Quantidade = reader.GetInt32("qtd_itens");
                dentro.Total = reader.GetDecimal("vl_total");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
    }
}
