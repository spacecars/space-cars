﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Space_cars_teste.DB.Base;
using Space_cars_teste.nsfspace_Car_DTO_s.Módulo_de_Compra;

namespace Space_cars_teste.nsfSpace_Car_Database.Módulo_de_Compra
{
   public class Database_CompraItem
    {

        public int Salvar(nsfspace_Car_DTO_s.Módulo_Compras.DTO_CompraItem dto)
        {
            string script =
            @"INSERT INTO tb_compra_item 
            (
                id_compra,
                id_produto
            )
            VALUES
            (
                @id_compra,
                @id_produto
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_compra", dto.ID_Compra));
            parm.Add(new MySqlParameter("id_produto", dto.ID_Produto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        internal int Salvar(DTO_CompraItem dto)
        {
            throw new NotImplementedException();
        }
    }
}
