﻿using MySql.Data.MySqlClient;
using Space_cars_teste.DB.Base;
using System.Collections.Generic;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Estoque
{
    public class Database_Estoque
    {
        public void Salvar(DTO_Estoque dto)
        {
            string script =
            @"INSERT INTO tb_estoque
            (
                id_produto,
                id_compra_item
            )
            VALUES
            (
                @id_produto,
                @id_compra_item
            )";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.ID_Produto));
            parms.Add(new MySqlParameter("id_compra_item", dto.ID_Pedido));
            
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_Estoque> Listar()
        {
            string script = @"SELECT * FROM tb_estoque";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<DTO_Estoque> lista = new List<DTO_Estoque>();

            while (reader.Read())
            {
                DTO_Estoque dto = new DTO_Estoque();
                dto.ID = reader.GetInt32("id_estoque");
                dto.ID_Produto = reader.GetInt32("id_produto");
                dto.ID_Pedido = reader.GetInt32("id_compra_item");
                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
