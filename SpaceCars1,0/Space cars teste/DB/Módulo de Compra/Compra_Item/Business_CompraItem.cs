﻿using Nsf.PuroTempero.DATABASE.Módulo_Compras;
using Nsf.PuroTempero.MODELO.Módulo_Compras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.BUSINESS.Módulo_Compras
{
    public class Business_CompraItem
    {
        public int Salvar(DTO_CompraItem dto)
        {
            Database_CompraItem db = new Database_CompraItem();
            return db.Salvar(dto);
        }
    }
}
