﻿using MySql.Data.MySqlClient;
using Nsf.PuroTempero.MODELO.Módulo_Compras;
using Space_cars_teste.DB.Base;
using System;
using System.Collections.Generic;

namespace Nsf.PuroTempero.DATABASE.Módulo_Compras
{
    public class VIEW_Compra
    {
        public List<VIEW_CompraDTO> Consultar(DateTime comeco, DateTime fim)
        {
            string script = @"SELECT * FROM view_consultar_pedidos
                                      WHERE dt_compra >= @comeco
                                        AND dt_compra <= @fim";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("comeco", comeco));
            parm.Add(new MySqlParameter("fim", fim));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<VIEW_CompraDTO> fora = new List<VIEW_CompraDTO>();

            while (reader.Read())
            {
                VIEW_CompraDTO dentro = new VIEW_CompraDTO();
                dentro.ID = reader.GetInt32("id_compra");
                dentro.Data = reader.GetDateTime("dt_compra");
                dentro.Fornecedor = reader.GetString("ds_razao_social");
                dentro.Quantidade = reader.GetInt32("qtd_itens");
                dentro.Total = reader.GetDecimal("vl_total");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
    }
}
