﻿using MySql.Data.MySqlClient;
using Space_cars_teste.DB.Base;
using System.Collections.Generic;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Cliente
{
    public class Database_Cliente
    {
        public int Salvar(DTO_Cliente dto)
        {
            string script =
            @"INSERT INTO tb_cliente
            (
                nm_cliente,
                ds_telefone,
                ds_celular,
                dt_nascimento,
                ds_cpf,
                ds_rg,
                ds_sexo,
                nm_prato_favorito,
                ds_frequencia,
                img_foto,
                ds_mesa_favorita,      
                ds_email,
                ds_endereco,
                ds_cep,
                ds_complemento,
                nr_casa,
                ds_uf,
                ds_cidade
            )
            VALUES
            (
                @nm_cliente,
                @ds_telefone,
                @ds_celular,
                @dt_nascimento,
                @ds_cpf,
                @ds_rg,
                @ds_sexo,
                @nm_prato_favorito,
                @ds_frequencia,
                @img_foto,
                @ds_mesa_favorita,      
                @ds_email,
                @ds_endereco,
                @ds_cep,
                @ds_complemento,
                @nr_casa,
                @ds_uf,
                @ds_cidade       
            )";
          

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_celular", dto.Celular));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("nm_prato_favorito", dto.PratoFavorito));
            parms.Add(new MySqlParameter("ds_frequencia", dto.Frequencia));
            parms.Add(new MySqlParameter("img_foto", dto.Imagem));
            parms.Add(new MySqlParameter("ds_mesa_favorita", dto.MesaFavorita));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("nr_casa", dto.Casa));
            parms.Add(new MySqlParameter("ds_uf", dto.UF));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);


        }

        public void Alterar(DTO_Cliente dto)
        {
            string script = @"UPDATE tb_cliente 
                                 SET nm_cliente             =@nm_cliente,                        
                                     ds_telefone            =@ds_telefone,
                                     ds_celular             =@ds_celular,
                                     dt_nascimento          =@dt_nascimento,
                                     ds_cpf                 =@ds_cpf,
                                     ds_rg                  =@ds_rg,
                                     ds_sexo                =@ds_sexo,
                                     nm_prato_favorito      =@nm_prato_favorito,
                                     ds_frequencia          =@ds_frequencia,
                                     img_foto               =@img_foto,
                                     ds_mesa_favorita       =@ds_mesa_favorita,
                                     ds_email               =@ds_email,
                                     ds_endereco            =@ds_endereco,
                                     ds_cep                 =@ds_cep,
                                     ds_complemento         =@ds_complemento,
                                     nr_casa                =@nr_casa,
                                     ds_uf                  =@ds_uf,
                                     ds_cidade              =@ds_cidade
                               WHERE id_cliente             =@id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.ID_Cliente));
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_celular", dto.Celular));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("nm_prato_favorito", dto.PratoFavorito));
            parms.Add(new MySqlParameter("ds_frequencia", dto.Frequencia));
            parms.Add(new MySqlParameter("img_foto", dto.Imagem));
            parms.Add(new MySqlParameter("ds_mesa_favorita", dto.MesaFavorita));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("nr_casa", dto.Casa));
            parms.Add(new MySqlParameter("ds_uf", dto.UF));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(DTO_Cliente dto)
        {
            string script = @"DELETE FROM tb_cliente
                                    WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_cliente", dto.ID_Cliente));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }

        public List<DTO_Cliente> Listar()
        {
            string script = @"SELECT * FROM  tb_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Cliente> Lista = new List<DTO_Cliente>();

            while (reader.Read())
            {
                DTO_Cliente dto = new DTO_Cliente();
                dto.ID_Cliente = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Celular = reader.GetString("ds_celular");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");
                dto.CPF = reader.GetString("ds_cpf");
                dto.RG = reader.GetString("ds_rg");
                dto.Sexo = reader.GetBoolean("ds_sexo");
                dto.PratoFavorito = reader.GetString("nm_prato_favorito");
                dto.Frequencia = reader.GetString("ds_frequencia");
                dto.Imagem = reader.GetString("img_foto");
                dto.MesaFavorita = reader.GetString("ds_mesa_favorita");
                dto.Email = reader.GetString("ds_email");
                dto.Endereco = reader.GetString("ds_endereco");
                dto.CEP = reader.GetString("ds_cep");
                dto.Complemento= reader.GetString("ds_complemento");
                dto.Casa = reader.GetInt32("nr_casa");
                dto.UF = reader.GetString("ds_uf");
                dto.Cidade = reader.GetString("ds_cidade");

                Lista.Add(dto);
            }

            reader.Close();
            return Lista;
        }

        public List<DTO_Cliente> Consultar (DTO_Cliente dt)
        {
            string script = @"SELECT * FROM tb_cliente 
                                      WHERE nm_cliente LIKE @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + dt.Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Cliente> Lista = new List<DTO_Cliente>();

            while (reader.Read())
            {
                DTO_Cliente dto = new DTO_Cliente();
                dto.ID_Cliente = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Celular = reader.GetString("ds_celular");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");
                dto.CPF = reader.GetString("ds_cpf");
                dto.RG = reader.GetString("ds_rg");
                dto.Sexo = reader.GetBoolean("ds_sexo");
                dto.PratoFavorito = reader.GetString("nm_prato_favorito");
                dto.Frequencia = reader.GetString("ds_frequencia");
                dto.Imagem = reader.GetString("img_foto");
                dto.MesaFavorita = reader.GetString("ds_mesa_favorita");
                dto.Email = reader.GetString("ds_email");
                dto.Endereco = reader.GetString("ds_endereco");
                dto.CEP = reader.GetString("ds_cep");
                dto.Complemento = reader.GetString("ds_complemento");
                dto.Casa = reader.GetInt32("nr_casa");
                dto.UF = reader.GetString("ds_uf");
                dto.Cidade = reader.GetString("ds_cidade");

                Lista.Add(dto);
            }

            reader.Close();
            return Lista;
        }
    }
}
