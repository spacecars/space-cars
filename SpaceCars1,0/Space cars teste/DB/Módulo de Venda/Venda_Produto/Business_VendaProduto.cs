﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Produtos
{
    public class Business_VendaProduto
    {
        Database_VendaProduto db = new Database_VendaProduto();

        public int Salvar(DTO_VendaProduto dto)
        {
            return db.Salvar(dto);
        }
        public void Remover(DTO_VendaProduto dto)
        {
            db.Remover(dto);
        }
        public void Alterar(DTO_VendaProduto dto)
        {
            db.Alterar(dto);
        }
        public List<DTO_VendaProduto> Listar()
        {
            List<DTO_VendaProduto> list = db.Listar();
            return list;
        }
        public List<DTO_VendaProduto> ConsultarPorProduto(DTO_VendaProduto dto)
        {
            List<DTO_VendaProduto> consult =  db.ConsutarPorProduto(dto);
            return consult;
        }

    }
}
