﻿using MySql.Data.MySqlClient;
using Space_cars_teste.DB.Base;
using System.Collections.Generic;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Produtos
{
    public class Database_VendaProduto
    {
        public int Salvar(DTO_VendaProduto dto)
        {
            string script = 
            @"INSERT INTO tb_produto
            (
                nm_produto, 
                vl_preco, 
                nm_marca
            )
            VALUES
            (
                @nm_produto, 
                @vl_preco, 
                @nm_marca
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));
            parms.Add(new MySqlParameter("nm_marca", dto.Marca));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(DTO_VendaProduto dto)
        {
            string script = @"UPDATE tb_produto 
                                 SET nm_produto =@nm_produto,
                                     vl_preco   =@vl_preco,
                                     nm_marca   =@nm_marca 
                               WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));
            parms.Add(new MySqlParameter("nm_marca", dto.Marca));
            parms.Add(new MySqlParameter("id_produto", dto.ID));

            Database db = new Database();
            db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(DTO_VendaProduto dto)
        {
            string script = @"DELETE FROM tb_produto 
                                    WHERE id_produto = id_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.ID));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public List<DTO_VendaProduto> Listar()
        {
            string script = @"SELECT * FROM tb_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_VendaProduto> lista = new List<DTO_VendaProduto>();

            while (reader.Read())
            {
                DTO_VendaProduto dto = new DTO_VendaProduto();
                dto.Nome = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");
                dto.Marca = reader.GetString("nm_marca");
                lista.Add(dto);
            }

            reader.Close();
            return lista;
        }

        public List<DTO_VendaProduto> ConsutarPorProduto(DTO_VendaProduto dto)
        {
            string script = @"SELECT * FROM tb_produto 
                                      WHERE nm_produto = @nm_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_VendaProduto> lista = new List<DTO_VendaProduto>();

            while (reader.Read())
            {
                DTO_VendaProduto dt = new DTO_VendaProduto();
                dt.Nome = reader.GetString("nm_produto");
                dt.Preco = reader.GetDecimal("vl_preco");
                dt.Marca = reader.GetString("nm_marca");
                lista.Add(dt);
            }

            reader.Close();
            return lista;
        }
    }
}
