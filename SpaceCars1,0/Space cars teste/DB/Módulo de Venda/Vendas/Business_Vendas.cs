﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Vendas
{
    public class Business_Vendas
    {
        Database_Vendas db = new Database_Vendas();

        public int Salvar(DTO_Vendas dto)
        {
            return db.Salvar(dto);
        }
        public void Alterar(DTO_Vendas dto)
        {
            db.Alterar(dto);
        }
        public void Remover(DTO_Vendas dto)
        {
            db.Remover(dto);
        }
        public List<DTO_Vendas> Listar()
        {
            List<DTO_Vendas> list =  db.Listar();
            return list;
        }
        public List<DTO_Vendas> ConsultarCliente(DTO_Vendas dt)
        {
            List<DTO_Vendas> consult = db.Consultar(dt);
            return consult;
        }
    }
}
