﻿using MySql.Data.MySqlClient;
using Space_cars_teste.DB.Base;
using System.Collections.Generic;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Vendas
{
    public class Database_Vendas
    {
        public int Salvar(DTO_Vendas dto)
        {
            string script = 
            @"INSERT INTO tb_pedido
            (
                id_cliente, 
                dt_venda, 
                id_funcionario
            )
            VALUES
            (
                @id_cliente, 
                @dt_venda, 
                @id_funcionario
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.ID_Cliente));
            parms.Add(new MySqlParameter("id_funcionario", dto.ID_Funcionario));
            parms.Add(new MySqlParameter("dt_venda", dto.DataVenda));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Alterar(DTO_Vendas dto)
        {
            string script = @"UPDATE tb_pedido 
                                 SET id_cliente      =@id_cliente,
                                     dt_venda        =@dt_venda,
                                     id_funcionario  =@id_funcionario
                               WHERE id_pedido       = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.ID_Cliente));
            parms.Add(new MySqlParameter("id_funcionario", dto.ID_Funcionario));
            parms.Add(new MySqlParameter("dt_venda", dto.DataVenda));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(DTO_Vendas dto)
        {
            string script = @"DELETE FROM tb_pedido 
                                    WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", dto.ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_Vendas> Listar()
        {
            string script = @"SELECT * FROM tb_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Vendas> Lista = new List<DTO_Vendas>();

            while (reader.Read())
            {
                DTO_Vendas dto = new DTO_Vendas();
                dto.ID = reader.GetInt32("id_pedido");
                dto.ID_Cliente = reader.GetInt32("id_cliente");
                dto.ID_Funcionario = reader.GetInt32("id_funcionario");
                dto.DataVenda = reader.GetDateTime("dt_venda");

                Lista.Add(dto);
            }
            reader.Close();
            return Lista;
        }

        public List<DTO_Vendas> Consultar(DTO_Vendas dt)
        {
            string script = @"SELECT * FROM tb_pedido 
                                      WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dt.ID_Cliente));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Vendas> Lista = new List<DTO_Vendas>();

            while (reader.Read())
            {
                DTO_Vendas dto = new DTO_Vendas();
                dto.ID = reader.GetInt32("id_pedido");
                dto.ID_Cliente = reader.GetInt32("id_cliente");
                dto.ID_Funcionario = reader.GetInt32("id_funcionario");
                dto.DataVenda = reader.GetDateTime("dt_venda");

                Lista.Add(dto);
            }
            reader.Close();
            return Lista;
        }

    }
}
