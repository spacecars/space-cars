﻿using Space_cars_teste.DB.Modelo_de_acesso.Database;
using Space_cars_teste.DB.Modelo_de_acesso.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.DB.Modelo_de_acesso.Business
{
    public class Business_Acesso
    {
        Database_Acesso db = new Database_Acesso();

        public int Salvar(DTO_Acesso dto)
        {
            return db.Salvar(dto);
        }

        public void Alterar (DTO_Acesso dto)
        {
            db.Alterar(dto);
        }

        public void Remover(DTO_Acesso dto)
        {
            db.Remover(dto);
        }

        public List<DTO_Acesso> Consultar (DTO_Acesso dto)
        {
            return db.Consultar(dto);
        }
    }
}
