﻿using MySql.Data.MySqlClient;
using Space_cars_teste.DB.Modelo_de_acesso.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.DB.Modelo_de_acesso.Database
{
    public class Database_Acesso
    {
        public int Salvar (DTO_Acesso dto)
        {
            string script = @"INSERT INTO tb_nivel_acesso 
                   (id_funcionario, id_cargo, bl_salvar, bl_remover, bl_alterar, bl_consultar)
            VALUES (@id_funcionario, @id_cargo, @bl_salvar, @bl_remover, @bl_alterar, @bl_consultar)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.ID_Funcionario));
            parms.Add(new MySqlParameter("id_cargo", dto.ID_Cargo));
            parms.Add(new MySqlParameter("bl_salvar", dto.Salvar));
            parms.Add(new MySqlParameter("bl_remover", dto.Remover));
            parms.Add(new MySqlParameter("bl_alterar", dto.Alterar));
            parms.Add(new MySqlParameter("bl_consultar", dto.Consultar));

            DB.Base.Database db = new DB.Base.Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar (DTO_Acesso dto)
        {
            string script =
                @"UPDATE tb_nvel_acesso SET id_funcionario = @id_funcionario,
                                            id_cargo = @id_cargo,
                                            bl_salvar = @bl_salvar,
                                            bl_remover = @bl_remover,
                                            bl_alterar =  @bl_alterar,
                                            bl_consultar = @consultar
                                      WHERE id_nivel_acesso =  @id_nivel_acesso";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_nivel_acesso", dto.ID));
            parms.Add(new MySqlParameter("id_funcioanrio", dto.ID_Funcionario));
            parms.Add(new MySqlParameter("id_cargo", dto.ID_Cargo));
            parms.Add(new MySqlParameter("bl_salvar", dto.Salvar));
            parms.Add(new MySqlParameter("bl_remover", dto.Remover));
            parms.Add(new MySqlParameter("bl_alterar", dto.Alterar));
            parms.Add(new MySqlParameter("bl_consultar", dto.Consultar));

            DB.Base.Database db = new DB.Base.Database();
            db.ExecuteInsertScript (script, parms);
        }

        public void Remover (DTO_Acesso dto)
        {
            string script =
                @"DELETAR FROM tb_nivel_acesso WHERE id_nivel_acesso = @id_nivel_acesso";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_nivel_acesso", dto.ID));

            DB.Base.Database db = new DB.Base.Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_Acesso> Consultar(DTO_Acesso dto)
        {
            string script =
                @"SELECT * FROM tb_nivel_acesso WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.ID_Funcionario));

            DB.Base.Database Database = new DB.Base.Database();
            MySqlDataReader reader = Database.ExecuteSelectScript(script, parms);

            List<DTO_Acesso> lista = new List<DTO_Acesso>();

            while(reader.Read())
            {
                DTO_Acesso dt = new DTO_Acesso();
                dt.ID = reader.GetInt32("id_nivel_acesso");
                dt.ID_Funcionario = reader.GetInt32("id_funcioanrio");
                dt.Salvar = reader.GetBoolean("bl_salvar");
                dt.Remover = reader.GetBoolean("bl_remover");
                dt.Alterar = reader.GetBoolean("bl_alterar");
                dt.Consultar = reader.GetBoolean("bl_consular");
                lista.Add(dt);
            }

            reader.Close();
            return lista;
        }

        public DTO_Funcionario Logar(DTO_Funcionario dto)
        {
            string script = @"SELECT * FROM tb_funcionario
                                      WHERE ds_usuario =@ds_usuario
                                      AND   ds_senha   =@ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));

            DB.Base.Database db = new DB.Base.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            DTO_Funcionario fora = null;

            if (reader.Read())
            {
                fora = new DTO_Funcionario();
                fora.Id = reader.GetInt32("id_funcionario");
                fora.Nome = reader.GetString("nm_funcionario");
                fora.RG = reader.GetString("ds_rg");
                fora.CPF = reader.GetString("ds_cpf");
                fora.Nascimento = reader.GetDateTime("dt_nascimento");
                fora.Sexo = reader.GetString("tp_sexo");
                fora.Transporte = reader.GetDecimal("vl_vale_transporte");
                fora.Refeicao = reader.GetDecimal("vl_vale_refeicao");
                fora.Alimentacao = reader.GetDecimal("vl_vale_alimentacao");
                fora.Convenio = reader.GetDecimal("vl_vale_convenio");
                fora.Salario = reader.GetDecimal("vl_salario");
                fora.Foto = reader.GetString("img_foto");
                fora.Complemento = reader.GetString("ds_complemento");
                fora.Usuario = reader.GetString("ds_usuario");
                fora.Senha = reader.GetString("ds_senha");
                fora.UF = reader.GetString("nm_uf");
                fora.Cidade = reader.GetString("nm_cidade");
                fora.Endereço = reader.GetString("nm_endereco");
                fora.Numero = reader.GetInt32("nr_residencia");
                fora.CEP = reader.GetString("ds_cep");
                fora.Complemento = reader.GetString("ds_complemento");
                fora.Email = reader.GetString("ds_email");
                fora.Telefone = reader.GetString("ds_telefone");
                fora.Celular = reader.GetString("ds_celular");
            }

            reader.Close();
            return fora;
        }

        //Servindo aqui como meio de descriptografar as senhas.
        public bool VerificarSenha(string senha)
        {
            string script = @"SELECT * FROM tb_funcionario
                                       WHERE ds_senha = @ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_senha", senha));

            DB.Base.Database db = new DB.Base.Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
