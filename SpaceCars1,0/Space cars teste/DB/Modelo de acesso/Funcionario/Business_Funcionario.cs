﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Validadores;
using Space_cars_teste.DB.Modelo_de_acesso.Database;
using Space_cars_teste.DB.Modelo_de_acesso.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Space_cars_teste.DB.Modelo_de_acesso.Business
{
    public class Business_Funcionario
    {
        Database_Funcionario db = new Database_Funcionario();

        public DTO_Funcionario Logar(DTO_Funcionario dto)
        {
            return db.Logar(dto);
        }
        public bool VerificarSenha(string senha)
        {
            return db.VerificarSenha(senha);
        }
        public int Salvar(DTO_Funcionario dto)
        {
            // Nome Nulo
            if (dto.Nome == null)
            {
                throw new ArgumentException("O nome não pode ser nulo");
            }
            else
            {
                // Nome sem número

                Validacao validacao = new Validacao();
                validacao.contemLetras(dto.Nome);
                //if (resp == false)
                //{
                //    throw new ArgumentException("O campo nome não aceita valores númericos");
                //}
                //else
                //{
                //    if(dto.Sexo == null)
                //    {
                //        throw new ArgumentException("O Sexo não pode ser nulo");

                //    }
                //    else
                //    {
                //        if(dto.Salario)
                //    }

                //}
                return db.Salvar(dto);
            }





        }
        public void Alterar(DTO_Funcionario dto)
        {
            db.Alterar(dto);
        }
        public void Remover(DTO_Funcionario dto)
        {
            db.Remover(dto);

        }
        public List<DTO_Funcionario> Consultar(DTO_Funcionario dto)
        {
            return db.Consultar(dto);
        }
    }
}
