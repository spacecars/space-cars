﻿using Space_cars_teste.DB.Modelo_de_acesso.Business;
using Space_cars_teste.DB.Modelo_de_acesso.DTO;
using Space_cars_teste.Forms.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Space_cars_teste.User_Controls.Módulo_de_Acesso.Funcionario
{
    public partial class frmConsultarFuncionario : Form
    {
        public frmConsultarFuncionario()
        {
            InitializeComponent();
        }

        private void lblNome_Click(object sender, EventArgs e)
        {

        }

        private void ConsultarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void btn_Buscar_Click(object sender, EventArgs e)
        {
            DTO_Funcionario dto = new DTO_Funcionario();
            dto.Nome = txtNome.Text;
            if (txtCPF.Text == "  .   .   /    -")
            {
                dto.CPF = "";
            }
            else
            {
                dto.CPF = txtCPF.Text;
            }
            if (txtRG.Text == "  .   .   -")
            {
                dto.RG = "";
            }
            else
            {
                dto.RG = txtRG.Text;
            }
            Business_Funcionario business = new Business_Funcionario();
            List<DTO_Funcionario> lista = new List<DTO_Funcionario>();
            lista = business.Consultar(dto);
            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = lista;
        }

        private void dgvFuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                DTO_Funcionario dto_funcionario = dgvFuncionario.Rows[e.RowIndex].DataBoundItem as DTO_Funcionario;

                DialogResult resultado = MessageBox.Show("Deseja realmente excluir?", "Puro-Tempero", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resultado == DialogResult.Yes)
                {
                    // Remover Acesso(antes do funcionario por causa da pk)

                    DTO_Acesso dto_acesso = new DTO_Acesso();
                    dto_acesso.ID_Funcionario = dto_funcionario.Id;

                    Business_Acesso b = new Business_Acesso();
                    b.Remover(dto_acesso);

                    //Remover Funcionário

                    Business_Funcionario business = new Business_Funcionario();
                    business.Remover(dto_funcionario);
                }
            }
            if (e.ColumnIndex == 8)
            {
                DTO_Funcionario dto_funcionario = dgvFuncionario.Rows[e.RowIndex].DataBoundItem as DTO_Funcionario;
                frmAlterarFuncionario frm = new frmAlterarFuncionario();
                frm.Loadscreen(dto_funcionario);
                //frm.ShowDialog();

            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            this.Hide();
        }
    }
}
