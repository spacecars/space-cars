﻿namespace Space_cars_teste.User_Controls.Módulo_de_Acesso.Funcionario
{
    partial class CadastrarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbDadosPessoais = new System.Windows.Forms.GroupBox();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.lblCelular = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.btnProcurar = new System.Windows.Forms.Button();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.chkM = new System.Windows.Forms.RadioButton();
            this.chkF = new System.Windows.Forms.RadioButton();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.lblSexo = new System.Windows.Forms.Label();
            this.lblDataNascimento = new System.Windows.Forms.Label();
            this.lblRG = new System.Windows.Forms.Label();
            this.lblCPF = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.imgFuncionario = new System.Windows.Forms.PictureBox();
            this.gpbEndereco = new System.Windows.Forms.GroupBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.cboUF = new System.Windows.Forms.ComboBox();
            this.lblCidade = new System.Windows.Forms.Label();
            this.nudNumero = new System.Windows.Forms.NumericUpDown();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblUF = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.lblCompemento = new System.Windows.Forms.Label();
            this.lblBairro = new System.Windows.Forms.Label();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.gpbSalario = new System.Windows.Forms.GroupBox();
            this.nudConvenio = new System.Windows.Forms.NumericUpDown();
            this.nudVA = new System.Windows.Forms.NumericUpDown();
            this.nudVT = new System.Windows.Forms.NumericUpDown();
            this.nudVR = new System.Windows.Forms.NumericUpDown();
            this.lblVA = new System.Windows.Forms.Label();
            this.nudSalario = new System.Windows.Forms.NumericUpDown();
            this.lblVT = new System.Windows.Forms.Label();
            this.lblConvenio = new System.Windows.Forms.Label();
            this.lblSalario = new System.Windows.Forms.Label();
            this.lblVR = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.lblSenha = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.gpbVendas = new System.Windows.Forms.GroupBox();
            this.chkConsultarVendas = new System.Windows.Forms.CheckBox();
            this.chkSalvarVendas = new System.Windows.Forms.CheckBox();
            this.chkAlterarVendas = new System.Windows.Forms.CheckBox();
            this.chkRemoverVendas = new System.Windows.Forms.CheckBox();
            this.gpbFuncionario = new System.Windows.Forms.GroupBox();
            this.chkConsultarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkSalvarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkAlterarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkRemoverFuncionario = new System.Windows.Forms.CheckBox();
            this.gpbRH = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.chkConsultarRH = new System.Windows.Forms.CheckBox();
            this.chkSalvarRH = new System.Windows.Forms.CheckBox();
            this.chkAlterarRH = new System.Windows.Forms.CheckBox();
            this.chkRemoverRH = new System.Windows.Forms.CheckBox();
            this.gpbFinanceiro = new System.Windows.Forms.GroupBox();
            this.chkConsultarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkSalvarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkAlterarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkRemoverFinanceiro = new System.Windows.Forms.CheckBox();
            this.gpbCompras = new System.Windows.Forms.GroupBox();
            this.chkConsultarCompras = new System.Windows.Forms.CheckBox();
            this.chkSalvarCompras = new System.Windows.Forms.CheckBox();
            this.chkAlterarCompras = new System.Windows.Forms.CheckBox();
            this.chkRemoverCompras = new System.Windows.Forms.CheckBox();
            this.gpbEstoque = new System.Windows.Forms.GroupBox();
            this.chkConsultarEstoque = new System.Windows.Forms.CheckBox();
            this.chkSalvarEstoque = new System.Windows.Forms.CheckBox();
            this.chkAlterarEstoque = new System.Windows.Forms.CheckBox();
            this.chkRemoverEstoque = new System.Windows.Forms.CheckBox();
            this.gpbDadosPessoais.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFuncionario)).BeginInit();
            this.gpbEndereco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).BeginInit();
            this.panel1.SuspendLayout();
            this.gpbSalario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gpbVendas.SuspendLayout();
            this.gpbFuncionario.SuspendLayout();
            this.gpbRH.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.gpbFinanceiro.SuspendLayout();
            this.gpbCompras.SuspendLayout();
            this.gpbEstoque.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpbDadosPessoais
            // 
            this.gpbDadosPessoais.BackColor = System.Drawing.Color.Transparent;
            this.gpbDadosPessoais.Controls.Add(this.txtCelular);
            this.gpbDadosPessoais.Controls.Add(this.lblCelular);
            this.gpbDadosPessoais.Controls.Add(this.txtTelefone);
            this.gpbDadosPessoais.Controls.Add(this.lblTelefone);
            this.gpbDadosPessoais.Controls.Add(this.btnProcurar);
            this.gpbDadosPessoais.Controls.Add(this.imgFuncionario);
            this.gpbDadosPessoais.Controls.Add(this.dtpNascimento);
            this.gpbDadosPessoais.Controls.Add(this.txtRG);
            this.gpbDadosPessoais.Controls.Add(this.chkM);
            this.gpbDadosPessoais.Controls.Add(this.chkF);
            this.gpbDadosPessoais.Controls.Add(this.txtCPF);
            this.gpbDadosPessoais.Controls.Add(this.txtNome);
            this.gpbDadosPessoais.Controls.Add(this.lblSexo);
            this.gpbDadosPessoais.Controls.Add(this.lblDataNascimento);
            this.gpbDadosPessoais.Controls.Add(this.lblRG);
            this.gpbDadosPessoais.Controls.Add(this.lblCPF);
            this.gpbDadosPessoais.Controls.Add(this.lblNome);
            this.gpbDadosPessoais.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbDadosPessoais.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbDadosPessoais.Location = new System.Drawing.Point(12, 35);
            this.gpbDadosPessoais.Name = "gpbDadosPessoais";
            this.gpbDadosPessoais.Size = new System.Drawing.Size(398, 170);
            this.gpbDadosPessoais.TabIndex = 16;
            this.gpbDadosPessoais.TabStop = false;
            this.gpbDadosPessoais.Text = "Dados Pessoais ";
            // 
            // txtCelular
            // 
            this.txtCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtCelular.ForeColor = System.Drawing.Color.Black;
            this.txtCelular.Location = new System.Drawing.Point(70, 141);
            this.txtCelular.Mask = "(99) 99999-9999";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(90, 20);
            this.txtCelular.TabIndex = 28;
            // 
            // lblCelular
            // 
            this.lblCelular.AutoSize = true;
            this.lblCelular.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCelular.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblCelular.Location = new System.Drawing.Point(6, 146);
            this.lblCelular.Name = "lblCelular";
            this.lblCelular.Size = new System.Drawing.Size(54, 15);
            this.lblCelular.TabIndex = 29;
            this.lblCelular.Text = "Celular:";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtTelefone.ForeColor = System.Drawing.Color.Black;
            this.txtTelefone.Location = new System.Drawing.Point(70, 118);
            this.txtTelefone.Mask = "(99) 99999-9999";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(90, 20);
            this.txtTelefone.TabIndex = 28;
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblTelefone.Location = new System.Drawing.Point(6, 123);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(65, 15);
            this.lblTelefone.TabIndex = 29;
            this.lblTelefone.Text = "Telefone:";
            // 
            // btnProcurar
            // 
            this.btnProcurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcurar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcurar.Font = new System.Drawing.Font("Perpetua Titling MT", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnProcurar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnProcurar.Location = new System.Drawing.Point(288, 138);
            this.btnProcurar.Name = "btnProcurar";
            this.btnProcurar.Size = new System.Drawing.Size(100, 23);
            this.btnProcurar.TabIndex = 13;
            this.btnProcurar.Text = "Procurar";
            this.btnProcurar.UseVisualStyleBackColor = true;
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNascimento.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNascimento.Location = new System.Drawing.Point(144, 93);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(129, 22);
            this.dtpNascimento.TabIndex = 11;
            // 
            // txtRG
            // 
            this.txtRG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRG.ForeColor = System.Drawing.Color.Black;
            this.txtRG.Location = new System.Drawing.Point(184, 51);
            this.txtRG.Mask = "99,999,999-9";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(89, 20);
            this.txtRG.TabIndex = 8;
            // 
            // chkM
            // 
            this.chkM.AutoSize = true;
            this.chkM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.chkM.Location = new System.Drawing.Point(79, 75);
            this.chkM.Name = "chkM";
            this.chkM.Size = new System.Drawing.Size(34, 17);
            this.chkM.TabIndex = 10;
            this.chkM.TabStop = true;
            this.chkM.Text = "M";
            this.chkM.UseVisualStyleBackColor = true;
            // 
            // chkF
            // 
            this.chkF.AutoSize = true;
            this.chkF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.chkF.Location = new System.Drawing.Point(45, 75);
            this.chkF.Name = "chkF";
            this.chkF.Size = new System.Drawing.Size(31, 17);
            this.chkF.TabIndex = 9;
            this.chkF.TabStop = true;
            this.chkF.Text = "F";
            this.chkF.UseVisualStyleBackColor = true;
            // 
            // txtCPF
            // 
            this.txtCPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPF.ForeColor = System.Drawing.Color.Black;
            this.txtCPF.Location = new System.Drawing.Point(42, 51);
            this.txtCPF.Mask = "00,000,000/0000-00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(108, 20);
            this.txtCPF.TabIndex = 7;
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.ForeColor = System.Drawing.Color.Black;
            this.txtNome.Location = new System.Drawing.Point(59, 26);
            this.txtNome.MaxLength = 50;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(214, 20);
            this.txtNome.TabIndex = 6;
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblSexo.Location = new System.Drawing.Point(6, 77);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(40, 15);
            this.lblSexo.TabIndex = 4;
            this.lblSexo.Text = "Sexo:";
            // 
            // lblDataNascimento
            // 
            this.lblDataNascimento.AutoSize = true;
            this.lblDataNascimento.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataNascimento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblDataNascimento.Location = new System.Drawing.Point(6, 99);
            this.lblDataNascimento.Name = "lblDataNascimento";
            this.lblDataNascimento.Size = new System.Drawing.Size(132, 15);
            this.lblDataNascimento.TabIndex = 1;
            this.lblDataNascimento.Text = "Data de Nascimento:";
            // 
            // lblRG
            // 
            this.lblRG.AutoSize = true;
            this.lblRG.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRG.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblRG.Location = new System.Drawing.Point(156, 56);
            this.lblRG.Name = "lblRG";
            this.lblRG.Size = new System.Drawing.Size(28, 15);
            this.lblRG.TabIndex = 1;
            this.lblRG.Text = "RG:";
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblCPF.Location = new System.Drawing.Point(6, 56);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(33, 15);
            this.lblCPF.TabIndex = 1;
            this.lblCPF.Text = "CPF:";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblNome.Location = new System.Drawing.Point(6, 31);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(47, 15);
            this.lblNome.TabIndex = 0;
            this.lblNome.Text = "Nome:";
            // 
            // imgFuncionario
            // 
            this.imgFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgFuncionario.Image = global::Space_cars_teste.Properties.Resources.botao_voltar;
            this.imgFuncionario.Location = new System.Drawing.Point(288, 26);
            this.imgFuncionario.Name = "imgFuncionario";
            this.imgFuncionario.Size = new System.Drawing.Size(100, 97);
            this.imgFuncionario.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgFuncionario.TabIndex = 12;
            this.imgFuncionario.TabStop = false;
            // 
            // gpbEndereco
            // 
            this.gpbEndereco.Controls.Add(this.txtCidade);
            this.gpbEndereco.Controls.Add(this.cboUF);
            this.gpbEndereco.Controls.Add(this.lblCidade);
            this.gpbEndereco.Controls.Add(this.nudNumero);
            this.gpbEndereco.Controls.Add(this.txtCEP);
            this.gpbEndereco.Controls.Add(this.lblNumero);
            this.gpbEndereco.Controls.Add(this.lblUF);
            this.gpbEndereco.Controls.Add(this.txtComplemento);
            this.gpbEndereco.Controls.Add(this.lblCompemento);
            this.gpbEndereco.Controls.Add(this.lblBairro);
            this.gpbEndereco.Controls.Add(this.txtEndereco);
            this.gpbEndereco.Controls.Add(this.lblCEP);
            this.gpbEndereco.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbEndereco.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbEndereco.Location = new System.Drawing.Point(12, 205);
            this.gpbEndereco.Name = "gpbEndereco";
            this.gpbEndereco.Size = new System.Drawing.Size(398, 130);
            this.gpbEndereco.TabIndex = 17;
            this.gpbEndereco.TabStop = false;
            this.gpbEndereco.Text = "Endereço ";
            // 
            // txtCidade
            // 
            this.txtCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtCidade.ForeColor = System.Drawing.Color.Black;
            this.txtCidade.Location = new System.Drawing.Point(251, 76);
            this.txtCidade.MaxLength = 10000;
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(133, 20);
            this.txtCidade.TabIndex = 61;
            // 
            // cboUF
            // 
            this.cboUF.BackColor = System.Drawing.Color.White;
            this.cboUF.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboUF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboUF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.cboUF.ForeColor = System.Drawing.Color.Black;
            this.cboUF.FormattingEnabled = true;
            this.cboUF.Location = new System.Drawing.Point(38, 75);
            this.cboUF.Name = "cboUF";
            this.cboUF.Size = new System.Drawing.Size(138, 21);
            this.cboUF.TabIndex = 55;
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblCidade.Location = new System.Drawing.Point(192, 82);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(53, 15);
            this.lblCidade.TabIndex = 56;
            this.lblCidade.Text = "Cidade:";
            // 
            // nudNumero
            // 
            this.nudNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudNumero.ForeColor = System.Drawing.Color.Black;
            this.nudNumero.Location = new System.Drawing.Point(251, 50);
            this.nudNumero.Name = "nudNumero";
            this.nudNumero.Size = new System.Drawing.Size(133, 20);
            this.nudNumero.TabIndex = 54;
            this.nudNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCEP
            // 
            this.txtCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCEP.ForeColor = System.Drawing.Color.Black;
            this.txtCEP.Location = new System.Drawing.Point(42, 102);
            this.txtCEP.Mask = "00000-000";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(87, 20);
            this.txtCEP.TabIndex = 49;
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblNumero.Location = new System.Drawing.Point(215, 56);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(29, 15);
            this.lblNumero.TabIndex = 53;
            this.lblNumero.Text = "N° :";
            // 
            // lblUF
            // 
            this.lblUF.AutoSize = true;
            this.lblUF.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblUF.Location = new System.Drawing.Point(6, 82);
            this.lblUF.Name = "lblUF";
            this.lblUF.Size = new System.Drawing.Size(26, 15);
            this.lblUF.TabIndex = 52;
            this.lblUF.Text = "UF:";
            // 
            // txtComplemento
            // 
            this.txtComplemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.txtComplemento.ForeColor = System.Drawing.Color.Black;
            this.txtComplemento.Location = new System.Drawing.Point(108, 49);
            this.txtComplemento.MaxLength = 10000;
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(98, 20);
            this.txtComplemento.TabIndex = 50;
            // 
            // lblCompemento
            // 
            this.lblCompemento.AutoSize = true;
            this.lblCompemento.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompemento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblCompemento.Location = new System.Drawing.Point(6, 55);
            this.lblCompemento.Name = "lblCompemento";
            this.lblCompemento.Size = new System.Drawing.Size(96, 15);
            this.lblCompemento.TabIndex = 51;
            this.lblCompemento.Text = "Complemento:";
            // 
            // lblBairro
            // 
            this.lblBairro.AutoSize = true;
            this.lblBairro.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBairro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblBairro.Location = new System.Drawing.Point(6, 30);
            this.lblBairro.Name = "lblBairro";
            this.lblBairro.Size = new System.Drawing.Size(67, 15);
            this.lblBairro.TabIndex = 46;
            this.lblBairro.Text = "Endereço:";
            // 
            // txtEndereco
            // 
            this.txtEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereco.ForeColor = System.Drawing.Color.Black;
            this.txtEndereco.Location = new System.Drawing.Point(79, 25);
            this.txtEndereco.MaxLength = 70;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(305, 20);
            this.txtEndereco.TabIndex = 47;
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblCEP.Location = new System.Drawing.Point(6, 107);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(34, 15);
            this.lblCEP.TabIndex = 48;
            this.lblCEP.Text = "CEP:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-12, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(825, 34);
            this.panel1.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Perpetua Titling MT", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(256, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(344, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cadrastrar funcionario";
            // 
            // gpbSalario
            // 
            this.gpbSalario.Controls.Add(this.nudConvenio);
            this.gpbSalario.Controls.Add(this.nudVA);
            this.gpbSalario.Controls.Add(this.nudVT);
            this.gpbSalario.Controls.Add(this.nudVR);
            this.gpbSalario.Controls.Add(this.lblVA);
            this.gpbSalario.Controls.Add(this.nudSalario);
            this.gpbSalario.Controls.Add(this.lblVT);
            this.gpbSalario.Controls.Add(this.lblConvenio);
            this.gpbSalario.Controls.Add(this.lblSalario);
            this.gpbSalario.Controls.Add(this.lblVR);
            this.gpbSalario.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbSalario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbSalario.Location = new System.Drawing.Point(12, 335);
            this.gpbSalario.Name = "gpbSalario";
            this.gpbSalario.Size = new System.Drawing.Size(398, 107);
            this.gpbSalario.TabIndex = 19;
            this.gpbSalario.TabStop = false;
            this.gpbSalario.Text = "Base Salarial ";
            // 
            // nudConvenio
            // 
            this.nudConvenio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudConvenio.DecimalPlaces = 2;
            this.nudConvenio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudConvenio.ForeColor = System.Drawing.Color.Black;
            this.nudConvenio.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudConvenio.Location = new System.Drawing.Point(279, 51);
            this.nudConvenio.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudConvenio.Name = "nudConvenio";
            this.nudConvenio.Size = new System.Drawing.Size(102, 20);
            this.nudConvenio.TabIndex = 40;
            this.nudConvenio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudVA
            // 
            this.nudVA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVA.DecimalPlaces = 2;
            this.nudVA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVA.ForeColor = System.Drawing.Color.Black;
            this.nudVA.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVA.Location = new System.Drawing.Point(84, 77);
            this.nudVA.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVA.Name = "nudVA";
            this.nudVA.Size = new System.Drawing.Size(102, 20);
            this.nudVA.TabIndex = 40;
            this.nudVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudVT
            // 
            this.nudVT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVT.DecimalPlaces = 2;
            this.nudVT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVT.ForeColor = System.Drawing.Color.Black;
            this.nudVT.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVT.Location = new System.Drawing.Point(84, 53);
            this.nudVT.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVT.Name = "nudVT";
            this.nudVT.Size = new System.Drawing.Size(102, 20);
            this.nudVT.TabIndex = 40;
            this.nudVT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudVR
            // 
            this.nudVR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVR.DecimalPlaces = 2;
            this.nudVR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVR.ForeColor = System.Drawing.Color.Black;
            this.nudVR.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVR.Location = new System.Drawing.Point(279, 25);
            this.nudVR.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVR.Name = "nudVR";
            this.nudVR.Size = new System.Drawing.Size(102, 20);
            this.nudVR.TabIndex = 40;
            this.nudVR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblVA
            // 
            this.lblVA.AutoSize = true;
            this.lblVA.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblVA.Location = new System.Drawing.Point(6, 84);
            this.lblVA.Name = "lblVA";
            this.lblVA.Size = new System.Drawing.Size(28, 15);
            this.lblVA.TabIndex = 39;
            this.lblVA.Text = "VA:";
            // 
            // nudSalario
            // 
            this.nudSalario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudSalario.DecimalPlaces = 2;
            this.nudSalario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudSalario.ForeColor = System.Drawing.Color.Black;
            this.nudSalario.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudSalario.Location = new System.Drawing.Point(84, 27);
            this.nudSalario.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudSalario.Name = "nudSalario";
            this.nudSalario.Size = new System.Drawing.Size(102, 20);
            this.nudSalario.TabIndex = 40;
            this.nudSalario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblVT
            // 
            this.lblVT.AutoSize = true;
            this.lblVT.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVT.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblVT.Location = new System.Drawing.Point(6, 58);
            this.lblVT.Name = "lblVT";
            this.lblVT.Size = new System.Drawing.Size(27, 15);
            this.lblVT.TabIndex = 39;
            this.lblVT.Text = "VT:";
            // 
            // lblConvenio
            // 
            this.lblConvenio.AutoSize = true;
            this.lblConvenio.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConvenio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblConvenio.Location = new System.Drawing.Point(201, 60);
            this.lblConvenio.Name = "lblConvenio";
            this.lblConvenio.Size = new System.Drawing.Size(69, 15);
            this.lblConvenio.TabIndex = 39;
            this.lblConvenio.Text = "Convênio:";
            // 
            // lblSalario
            // 
            this.lblSalario.AutoSize = true;
            this.lblSalario.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSalario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblSalario.Location = new System.Drawing.Point(6, 32);
            this.lblSalario.Name = "lblSalario";
            this.lblSalario.Size = new System.Drawing.Size(53, 15);
            this.lblSalario.TabIndex = 39;
            this.lblSalario.Text = "Salário:";
            // 
            // lblVR
            // 
            this.lblVR.AutoSize = true;
            this.lblVR.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVR.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblVR.Location = new System.Drawing.Point(201, 32);
            this.lblVR.Name = "lblVR";
            this.lblVR.Size = new System.Drawing.Size(27, 15);
            this.lblVR.TabIndex = 39;
            this.lblVR.Text = "VR:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gpbEstoque);
            this.groupBox1.Controls.Add(this.gpbCompras);
            this.groupBox1.Controls.Add(this.gpbFinanceiro);
            this.groupBox1.Controls.Add(this.gpbRH);
            this.groupBox1.Controls.Add(this.gpbFuncionario);
            this.groupBox1.Controls.Add(this.gpbVendas);
            this.groupBox1.Controls.Add(this.txtSenha);
            this.groupBox1.Controls.Add(this.txtUsuario);
            this.groupBox1.Controls.Add(this.lblSenha);
            this.groupBox1.Controls.Add(this.lblUsuario);
            this.groupBox1.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.groupBox1.Location = new System.Drawing.Point(422, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(365, 300);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Acesso";
            // 
            // txtSenha
            // 
            this.txtSenha.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenha.ForeColor = System.Drawing.Color.Black;
            this.txtSenha.Location = new System.Drawing.Point(77, 56);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(137, 20);
            this.txtSenha.TabIndex = 39;
            this.txtSenha.UseSystemPasswordChar = true;
            // 
            // txtUsuario
            // 
            this.txtUsuario.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.ForeColor = System.Drawing.Color.Black;
            this.txtUsuario.Location = new System.Drawing.Point(77, 27);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(138, 20);
            this.txtUsuario.TabIndex = 38;
            // 
            // lblSenha
            // 
            this.lblSenha.AutoSize = true;
            this.lblSenha.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSenha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblSenha.Location = new System.Drawing.Point(16, 57);
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.Size = new System.Drawing.Size(47, 15);
            this.lblSenha.TabIndex = 37;
            this.lblSenha.Text = "Senha:";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Segoe UI Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblUsuario.Location = new System.Drawing.Point(16, 27);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(58, 15);
            this.lblUsuario.TabIndex = 36;
            this.lblUsuario.Text = "Usuário:";
            // 
            // gpbVendas
            // 
            this.gpbVendas.Controls.Add(this.chkConsultarVendas);
            this.gpbVendas.Controls.Add(this.chkSalvarVendas);
            this.gpbVendas.Controls.Add(this.chkAlterarVendas);
            this.gpbVendas.Controls.Add(this.chkRemoverVendas);
            this.gpbVendas.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbVendas.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbVendas.Location = new System.Drawing.Point(1, 78);
            this.gpbVendas.Name = "gpbVendas";
            this.gpbVendas.Size = new System.Drawing.Size(184, 75);
            this.gpbVendas.TabIndex = 62;
            this.gpbVendas.TabStop = false;
            this.gpbVendas.Text = "Vendas";
            // 
            // chkConsultarVendas
            // 
            this.chkConsultarVendas.AutoSize = true;
            this.chkConsultarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarVendas.Location = new System.Drawing.Point(18, 29);
            this.chkConsultarVendas.Name = "chkConsultarVendas";
            this.chkConsultarVendas.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarVendas.TabIndex = 37;
            this.chkConsultarVendas.Text = "Consultar";
            this.chkConsultarVendas.UseVisualStyleBackColor = true;
            // 
            // chkSalvarVendas
            // 
            this.chkSalvarVendas.AutoSize = true;
            this.chkSalvarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarVendas.Location = new System.Drawing.Point(104, 29);
            this.chkSalvarVendas.Name = "chkSalvarVendas";
            this.chkSalvarVendas.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarVendas.TabIndex = 38;
            this.chkSalvarVendas.Text = "Salvar";
            this.chkSalvarVendas.UseVisualStyleBackColor = true;
            // 
            // chkAlterarVendas
            // 
            this.chkAlterarVendas.AutoSize = true;
            this.chkAlterarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarVendas.Location = new System.Drawing.Point(18, 52);
            this.chkAlterarVendas.Name = "chkAlterarVendas";
            this.chkAlterarVendas.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarVendas.TabIndex = 39;
            this.chkAlterarVendas.Text = "Alterar";
            this.chkAlterarVendas.UseVisualStyleBackColor = true;
            // 
            // chkRemoverVendas
            // 
            this.chkRemoverVendas.AutoSize = true;
            this.chkRemoverVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverVendas.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverVendas.Location = new System.Drawing.Point(104, 52);
            this.chkRemoverVendas.Name = "chkRemoverVendas";
            this.chkRemoverVendas.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverVendas.TabIndex = 40;
            this.chkRemoverVendas.Text = "Remover";
            this.chkRemoverVendas.UseVisualStyleBackColor = true;
            // 
            // gpbFuncionario
            // 
            this.gpbFuncionario.Controls.Add(this.chkConsultarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkSalvarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkAlterarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkRemoverFuncionario);
            this.gpbFuncionario.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbFuncionario.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbFuncionario.Location = new System.Drawing.Point(183, 78);
            this.gpbFuncionario.Name = "gpbFuncionario";
            this.gpbFuncionario.Size = new System.Drawing.Size(181, 75);
            this.gpbFuncionario.TabIndex = 63;
            this.gpbFuncionario.TabStop = false;
            this.gpbFuncionario.Text = "Funcionário";
            // 
            // chkConsultarFuncionario
            // 
            this.chkConsultarFuncionario.AutoSize = true;
            this.chkConsultarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarFuncionario.Location = new System.Drawing.Point(18, 29);
            this.chkConsultarFuncionario.Name = "chkConsultarFuncionario";
            this.chkConsultarFuncionario.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarFuncionario.TabIndex = 37;
            this.chkConsultarFuncionario.Text = "Consultar";
            this.chkConsultarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkSalvarFuncionario
            // 
            this.chkSalvarFuncionario.AutoSize = true;
            this.chkSalvarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarFuncionario.Location = new System.Drawing.Point(104, 29);
            this.chkSalvarFuncionario.Name = "chkSalvarFuncionario";
            this.chkSalvarFuncionario.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarFuncionario.TabIndex = 38;
            this.chkSalvarFuncionario.Text = "Salvar";
            this.chkSalvarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFuncionario
            // 
            this.chkAlterarFuncionario.AutoSize = true;
            this.chkAlterarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarFuncionario.Location = new System.Drawing.Point(19, 52);
            this.chkAlterarFuncionario.Name = "chkAlterarFuncionario";
            this.chkAlterarFuncionario.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarFuncionario.TabIndex = 39;
            this.chkAlterarFuncionario.Text = "Alterar";
            this.chkAlterarFuncionario.UseVisualStyleBackColor = true;
            this.chkAlterarFuncionario.CheckedChanged += new System.EventHandler(this.chkAlterarFuncionario_CheckedChanged);
            // 
            // chkRemoverFuncionario
            // 
            this.chkRemoverFuncionario.AutoSize = true;
            this.chkRemoverFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverFuncionario.Location = new System.Drawing.Point(105, 52);
            this.chkRemoverFuncionario.Name = "chkRemoverFuncionario";
            this.chkRemoverFuncionario.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverFuncionario.TabIndex = 40;
            this.chkRemoverFuncionario.Text = "Remover";
            this.chkRemoverFuncionario.UseVisualStyleBackColor = true;
            this.chkRemoverFuncionario.CheckedChanged += new System.EventHandler(this.chkRemoverFuncionario_CheckedChanged);
            // 
            // gpbRH
            // 
            this.gpbRH.Controls.Add(this.groupBox4);
            this.gpbRH.Controls.Add(this.chkConsultarRH);
            this.gpbRH.Controls.Add(this.chkSalvarRH);
            this.gpbRH.Controls.Add(this.chkAlterarRH);
            this.gpbRH.Controls.Add(this.chkRemoverRH);
            this.gpbRH.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbRH.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbRH.Location = new System.Drawing.Point(1, 155);
            this.gpbRH.Name = "gpbRH";
            this.gpbRH.Size = new System.Drawing.Size(185, 70);
            this.gpbRH.TabIndex = 64;
            this.gpbRH.TabStop = false;
            this.gpbRH.Text = "RH";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkBox5);
            this.groupBox4.Controls.Add(this.checkBox6);
            this.groupBox4.Controls.Add(this.checkBox7);
            this.groupBox4.Controls.Add(this.checkBox8);
            this.groupBox4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.groupBox4.ForeColor = System.Drawing.Color.DarkGreen;
            this.groupBox4.Location = new System.Drawing.Point(215, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(176, 94);
            this.groupBox4.TabIndex = 64;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "RH";
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox5.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox5.ForeColor = System.Drawing.Color.Black;
            this.checkBox5.Location = new System.Drawing.Point(18, 35);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(76, 17);
            this.checkBox5.TabIndex = 37;
            this.checkBox5.Text = "Consultar";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox6.ForeColor = System.Drawing.Color.Black;
            this.checkBox6.Location = new System.Drawing.Point(104, 35);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(57, 17);
            this.checkBox6.TabIndex = 38;
            this.checkBox6.Text = "Salvar";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox7.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox7.ForeColor = System.Drawing.Color.Black;
            this.checkBox7.Location = new System.Drawing.Point(18, 64);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(61, 17);
            this.checkBox7.TabIndex = 39;
            this.checkBox7.Text = "Alterar";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox8.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox8.ForeColor = System.Drawing.Color.Black;
            this.checkBox8.Location = new System.Drawing.Point(104, 64);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(72, 17);
            this.checkBox8.TabIndex = 40;
            this.checkBox8.Text = "Remover";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // chkConsultarRH
            // 
            this.chkConsultarRH.AutoSize = true;
            this.chkConsultarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarRH.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarRH.Location = new System.Drawing.Point(18, 25);
            this.chkConsultarRH.Name = "chkConsultarRH";
            this.chkConsultarRH.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarRH.TabIndex = 37;
            this.chkConsultarRH.Text = "Consultar";
            this.chkConsultarRH.UseVisualStyleBackColor = true;
            // 
            // chkSalvarRH
            // 
            this.chkSalvarRH.AutoSize = true;
            this.chkSalvarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarRH.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarRH.Location = new System.Drawing.Point(104, 25);
            this.chkSalvarRH.Name = "chkSalvarRH";
            this.chkSalvarRH.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarRH.TabIndex = 38;
            this.chkSalvarRH.Text = "Salvar";
            this.chkSalvarRH.UseVisualStyleBackColor = true;
            // 
            // chkAlterarRH
            // 
            this.chkAlterarRH.AutoSize = true;
            this.chkAlterarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarRH.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarRH.Location = new System.Drawing.Point(18, 48);
            this.chkAlterarRH.Name = "chkAlterarRH";
            this.chkAlterarRH.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarRH.TabIndex = 39;
            this.chkAlterarRH.Text = "Alterar";
            this.chkAlterarRH.UseVisualStyleBackColor = true;
            // 
            // chkRemoverRH
            // 
            this.chkRemoverRH.AutoSize = true;
            this.chkRemoverRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverRH.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverRH.Location = new System.Drawing.Point(104, 48);
            this.chkRemoverRH.Name = "chkRemoverRH";
            this.chkRemoverRH.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverRH.TabIndex = 40;
            this.chkRemoverRH.Text = "Remover";
            this.chkRemoverRH.UseVisualStyleBackColor = true;
            // 
            // gpbFinanceiro
            // 
            this.gpbFinanceiro.Controls.Add(this.chkConsultarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkSalvarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkAlterarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkRemoverFinanceiro);
            this.gpbFinanceiro.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbFinanceiro.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbFinanceiro.Location = new System.Drawing.Point(184, 155);
            this.gpbFinanceiro.Name = "gpbFinanceiro";
            this.gpbFinanceiro.Size = new System.Drawing.Size(180, 70);
            this.gpbFinanceiro.TabIndex = 65;
            this.gpbFinanceiro.TabStop = false;
            this.gpbFinanceiro.Text = "Financeiro";
            // 
            // chkConsultarFinanceiro
            // 
            this.chkConsultarFinanceiro.AutoSize = true;
            this.chkConsultarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarFinanceiro.Location = new System.Drawing.Point(17, 25);
            this.chkConsultarFinanceiro.Name = "chkConsultarFinanceiro";
            this.chkConsultarFinanceiro.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarFinanceiro.TabIndex = 37;
            this.chkConsultarFinanceiro.Text = "Consultar";
            this.chkConsultarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkSalvarFinanceiro
            // 
            this.chkSalvarFinanceiro.AutoSize = true;
            this.chkSalvarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarFinanceiro.Location = new System.Drawing.Point(103, 25);
            this.chkSalvarFinanceiro.Name = "chkSalvarFinanceiro";
            this.chkSalvarFinanceiro.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarFinanceiro.TabIndex = 38;
            this.chkSalvarFinanceiro.Text = "Salvar";
            this.chkSalvarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFinanceiro
            // 
            this.chkAlterarFinanceiro.AutoSize = true;
            this.chkAlterarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarFinanceiro.Location = new System.Drawing.Point(18, 48);
            this.chkAlterarFinanceiro.Name = "chkAlterarFinanceiro";
            this.chkAlterarFinanceiro.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarFinanceiro.TabIndex = 39;
            this.chkAlterarFinanceiro.Text = "Alterar";
            this.chkAlterarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkRemoverFinanceiro
            // 
            this.chkRemoverFinanceiro.AutoSize = true;
            this.chkRemoverFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverFinanceiro.Location = new System.Drawing.Point(104, 48);
            this.chkRemoverFinanceiro.Name = "chkRemoverFinanceiro";
            this.chkRemoverFinanceiro.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverFinanceiro.TabIndex = 40;
            this.chkRemoverFinanceiro.Text = "Remover";
            this.chkRemoverFinanceiro.UseVisualStyleBackColor = true;
            // 
            // gpbCompras
            // 
            this.gpbCompras.Controls.Add(this.chkConsultarCompras);
            this.gpbCompras.Controls.Add(this.chkSalvarCompras);
            this.gpbCompras.Controls.Add(this.chkAlterarCompras);
            this.gpbCompras.Controls.Add(this.chkRemoverCompras);
            this.gpbCompras.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbCompras.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbCompras.Location = new System.Drawing.Point(1, 226);
            this.gpbCompras.Name = "gpbCompras";
            this.gpbCompras.Size = new System.Drawing.Size(194, 73);
            this.gpbCompras.TabIndex = 66;
            this.gpbCompras.TabStop = false;
            this.gpbCompras.Text = "Compras";
            // 
            // chkConsultarCompras
            // 
            this.chkConsultarCompras.AutoSize = true;
            this.chkConsultarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarCompras.Location = new System.Drawing.Point(18, 25);
            this.chkConsultarCompras.Name = "chkConsultarCompras";
            this.chkConsultarCompras.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarCompras.TabIndex = 37;
            this.chkConsultarCompras.Text = "Consultar";
            this.chkConsultarCompras.UseVisualStyleBackColor = true;
            // 
            // chkSalvarCompras
            // 
            this.chkSalvarCompras.AutoSize = true;
            this.chkSalvarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarCompras.Location = new System.Drawing.Point(104, 25);
            this.chkSalvarCompras.Name = "chkSalvarCompras";
            this.chkSalvarCompras.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarCompras.TabIndex = 38;
            this.chkSalvarCompras.Text = "Salvar";
            this.chkSalvarCompras.UseVisualStyleBackColor = true;
            // 
            // chkAlterarCompras
            // 
            this.chkAlterarCompras.AutoSize = true;
            this.chkAlterarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarCompras.Location = new System.Drawing.Point(18, 48);
            this.chkAlterarCompras.Name = "chkAlterarCompras";
            this.chkAlterarCompras.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarCompras.TabIndex = 39;
            this.chkAlterarCompras.Text = "Alterar";
            this.chkAlterarCompras.UseVisualStyleBackColor = true;
            // 
            // chkRemoverCompras
            // 
            this.chkRemoverCompras.AutoSize = true;
            this.chkRemoverCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverCompras.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverCompras.Location = new System.Drawing.Point(104, 48);
            this.chkRemoverCompras.Name = "chkRemoverCompras";
            this.chkRemoverCompras.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverCompras.TabIndex = 40;
            this.chkRemoverCompras.Text = "Remover";
            this.chkRemoverCompras.UseVisualStyleBackColor = true;
            // 
            // gpbEstoque
            // 
            this.gpbEstoque.Controls.Add(this.chkConsultarEstoque);
            this.gpbEstoque.Controls.Add(this.chkSalvarEstoque);
            this.gpbEstoque.Controls.Add(this.chkAlterarEstoque);
            this.gpbEstoque.Controls.Add(this.chkRemoverEstoque);
            this.gpbEstoque.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbEstoque.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbEstoque.Location = new System.Drawing.Point(184, 226);
            this.gpbEstoque.Name = "gpbEstoque";
            this.gpbEstoque.Size = new System.Drawing.Size(180, 73);
            this.gpbEstoque.TabIndex = 67;
            this.gpbEstoque.TabStop = false;
            this.gpbEstoque.Text = "Estoque";
            // 
            // chkConsultarEstoque
            // 
            this.chkConsultarEstoque.AutoSize = true;
            this.chkConsultarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarEstoque.Location = new System.Drawing.Point(17, 23);
            this.chkConsultarEstoque.Name = "chkConsultarEstoque";
            this.chkConsultarEstoque.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarEstoque.TabIndex = 37;
            this.chkConsultarEstoque.Text = "Consultar";
            this.chkConsultarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkSalvarEstoque
            // 
            this.chkSalvarEstoque.AutoSize = true;
            this.chkSalvarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarEstoque.Location = new System.Drawing.Point(103, 23);
            this.chkSalvarEstoque.Name = "chkSalvarEstoque";
            this.chkSalvarEstoque.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarEstoque.TabIndex = 38;
            this.chkSalvarEstoque.Text = "Salvar";
            this.chkSalvarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkAlterarEstoque
            // 
            this.chkAlterarEstoque.AutoSize = true;
            this.chkAlterarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarEstoque.Location = new System.Drawing.Point(17, 48);
            this.chkAlterarEstoque.Name = "chkAlterarEstoque";
            this.chkAlterarEstoque.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarEstoque.TabIndex = 39;
            this.chkAlterarEstoque.Text = "Alterar";
            this.chkAlterarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkRemoverEstoque
            // 
            this.chkRemoverEstoque.AutoSize = true;
            this.chkRemoverEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverEstoque.Location = new System.Drawing.Point(103, 48);
            this.chkRemoverEstoque.Name = "chkRemoverEstoque";
            this.chkRemoverEstoque.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverEstoque.TabIndex = 40;
            this.chkRemoverEstoque.Text = "Remover";
            this.chkRemoverEstoque.UseVisualStyleBackColor = true;
            // 
            // CadastrarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(800, 456);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gpbSalario);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gpbEndereco);
            this.Controls.Add(this.gpbDadosPessoais);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CadastrarFuncionario";
            this.Text = "CadastrarFuncionario";
            this.gpbDadosPessoais.ResumeLayout(false);
            this.gpbDadosPessoais.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgFuncionario)).EndInit();
            this.gpbEndereco.ResumeLayout(false);
            this.gpbEndereco.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumero)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.gpbSalario.ResumeLayout(false);
            this.gpbSalario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gpbVendas.ResumeLayout(false);
            this.gpbVendas.PerformLayout();
            this.gpbFuncionario.ResumeLayout(false);
            this.gpbFuncionario.PerformLayout();
            this.gpbRH.ResumeLayout(false);
            this.gpbRH.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.gpbFinanceiro.ResumeLayout(false);
            this.gpbFinanceiro.PerformLayout();
            this.gpbCompras.ResumeLayout(false);
            this.gpbCompras.PerformLayout();
            this.gpbEstoque.ResumeLayout(false);
            this.gpbEstoque.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbDadosPessoais;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.Label lblCelular;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.Button btnProcurar;
        private System.Windows.Forms.PictureBox imgFuncionario;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.RadioButton chkM;
        private System.Windows.Forms.RadioButton chkF;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Label lblDataNascimento;
        private System.Windows.Forms.Label lblRG;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.GroupBox gpbEndereco;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.ComboBox cboUF;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.NumericUpDown nudNumero;
        private System.Windows.Forms.MaskedTextBox txtCEP;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblUF;
        private System.Windows.Forms.TextBox txtComplemento;
        private System.Windows.Forms.Label lblCompemento;
        private System.Windows.Forms.Label lblBairro;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label lblCEP;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gpbSalario;
        private System.Windows.Forms.NumericUpDown nudConvenio;
        private System.Windows.Forms.NumericUpDown nudVA;
        private System.Windows.Forms.NumericUpDown nudVT;
        private System.Windows.Forms.NumericUpDown nudVR;
        private System.Windows.Forms.Label lblVA;
        private System.Windows.Forms.NumericUpDown nudSalario;
        private System.Windows.Forms.Label lblVT;
        private System.Windows.Forms.Label lblConvenio;
        private System.Windows.Forms.Label lblSalario;
        private System.Windows.Forms.Label lblVR;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gpbEstoque;
        private System.Windows.Forms.CheckBox chkConsultarEstoque;
        private System.Windows.Forms.CheckBox chkSalvarEstoque;
        private System.Windows.Forms.CheckBox chkAlterarEstoque;
        private System.Windows.Forms.CheckBox chkRemoverEstoque;
        private System.Windows.Forms.GroupBox gpbCompras;
        private System.Windows.Forms.CheckBox chkConsultarCompras;
        private System.Windows.Forms.CheckBox chkSalvarCompras;
        private System.Windows.Forms.CheckBox chkAlterarCompras;
        private System.Windows.Forms.CheckBox chkRemoverCompras;
        private System.Windows.Forms.GroupBox gpbFinanceiro;
        private System.Windows.Forms.CheckBox chkConsultarFinanceiro;
        private System.Windows.Forms.CheckBox chkSalvarFinanceiro;
        private System.Windows.Forms.CheckBox chkAlterarFinanceiro;
        private System.Windows.Forms.CheckBox chkRemoverFinanceiro;
        private System.Windows.Forms.GroupBox gpbRH;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox chkConsultarRH;
        private System.Windows.Forms.CheckBox chkSalvarRH;
        private System.Windows.Forms.CheckBox chkAlterarRH;
        private System.Windows.Forms.CheckBox chkRemoverRH;
        private System.Windows.Forms.GroupBox gpbFuncionario;
        private System.Windows.Forms.CheckBox chkConsultarFuncionario;
        private System.Windows.Forms.CheckBox chkSalvarFuncionario;
        private System.Windows.Forms.CheckBox chkAlterarFuncionario;
        private System.Windows.Forms.CheckBox chkRemoverFuncionario;
        private System.Windows.Forms.GroupBox gpbVendas;
        private System.Windows.Forms.CheckBox chkConsultarVendas;
        private System.Windows.Forms.CheckBox chkSalvarVendas;
        private System.Windows.Forms.CheckBox chkAlterarVendas;
        private System.Windows.Forms.CheckBox chkRemoverVendas;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.Label lblSenha;
        private System.Windows.Forms.Label lblUsuario;
    }
}