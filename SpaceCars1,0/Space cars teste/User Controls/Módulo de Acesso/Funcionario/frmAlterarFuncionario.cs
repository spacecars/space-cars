﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas;
using Space_cars_teste.DB.Modelo_de_acesso.Business;
using Space_cars_teste.DB.Modelo_de_acesso.DTO;
using Space_cars_teste.Forms.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Space_cars_teste.User_Controls.Módulo_de_Acesso.Funcionario
{
    public partial class frmAlterarFuncionario : Form
    {
        public frmAlterarFuncionario()
        {
            InitializeComponent();
        }

        DTO_Funcionario dto = new DTO_Funcionario();

        public void Loadscreen(DTO_Funcionario dto)

        {
            this.dto = dto;
            txtNome.Text = dto.Nome;
            txtRG.Text = dto.RG;
            txtCPF.Text = dto.CPF;
            dtpNascimento.Value = dto.Nascimento;
            if (dto.Sexo == "F")
            {
                chkF.Checked = true;
            }
            if (dto.Sexo == "M")
            {
                chkM.Checked = true;
            }
            nudVT.Value = dto.Transporte;
            nudVR.Value = dto.Refeicao;
            nudVA.Value = dto.Alimentacao;
            nudConvenio.Value = dto.Convenio;
            nudSalario.Value = dto.Salario;
            if (dto.Foto == null)
            {

            }
            else
            {
                imgFuncionario.Image = ImagemPlugin.ConverterParaImagem(dto.Foto);

            }
            txtUsuario.Text = dto.Usuario;
            txtSenha.Text = dto.Senha;
            cboUF.SelectedItem = dto.UF;
            txtCidade.Text = dto.Cidade;
            txtEndereco.Text = dto.Endereço;
            nudNumero.Value = dto.Numero;
            txtCEP.Text = dto.CEP;
            txtComplemento.Text = dto.Complemento;
            txtTelefone.Text = dto.Telefone;
            txtCelular.Text = dto.Celular;
        }

        private void btnReCadastrar_Click(object sender, EventArgs e)
        {
            this.dto.Nome = txtNome.Text;

            // if para n salvar RG só com a Mask (Não mexer para n afetar outra parte)
            if (this.txtRG.Text == "  .   .   -")
            {
                this.txtRG.Text = null;
            }
            else
            {
                this.dto.RG = txtRG.Text;
            }
            // if para n salvar CPF só com a Mask (Não mexer para n afetar outra parte)


            if (this.txtRG.Text == "  .   .   /    -")
            {
                this.txtCPF.Text = null;
            }
            else
            {
                this.dto.CPF = txtCPF.Text;
            }

            this.dto.Nascimento = dtpNascimento.Value;
            if (this.chkF.Checked == true)
            {
                this.dto.Sexo = chkF.Text.ToString();
            }
            if (this.chkM.Checked == true)
            {
                this.dto.Sexo = chkM.Text.ToString();
            }
            this.dto.Transporte = nudVT.Value;
            this.dto.Refeicao = nudVR.Value;
            this.dto.Alimentacao = nudVA.Value;
            this.dto.Convenio = nudConvenio.Value;
            this.dto.Salario = nudSalario.Value;
            this.dto.Foto = ImagemPlugin.ConverterParaString(imgFuncionario.Image);
            this.dto.Usuario = txtUsuario.Text;
            this.dto.Senha = txtSenha.Text;
            this.dto.UF = cboUF.SelectedItem.ToString();
            this.dto.Cidade = txtCidade.Text;
            this.dto.Endereço = txtEndereco.Text;
            this.dto.Numero = Convert.ToInt32(nudNumero.Value);
            this.dto.CEP = txtCEP.Text;
            this.dto.Complemento = txtComplemento.Text;
            this.dto.Telefone = txtTelefone.Text;
            this.dto.Celular = txtCelular.Text;
            Business_Funcionario business = new Business_Funcionario();
            business.Alterar(dto);
        }

        private void frmAlterarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            frmInicial tela = new frmInicial();
            tela.Show();
            this.Hide();
        }

        private void gpbFinanceiro_Enter(object sender, EventArgs e)
        {

        }

        private void imgFuncionario_Click(object sender, EventArgs e)
        {

        }

        private void lblCompemento_Click(object sender, EventArgs e)
        {

        }

        private void gpbEndereco_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
