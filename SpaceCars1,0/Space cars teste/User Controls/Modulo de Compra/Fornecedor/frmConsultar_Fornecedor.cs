﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Space_cars_teste.nsfspace_Car_DTO_s.Módulo_de_Compra;
using Space_cars_teste.nsfSapce_Car_Business.Módulo_de_Compras;

namespace Space_cars_teste.User_Controls.Módulo_de_ompras.Fornecedor
{
    public partial class frmConsultar_Fornecedor : UserControl
    {
        private object dgvCompra;

        public frmConsultar_Fornecedor()
        {
            InitializeComponent();
            PermitirAcesso();
        }

        private void InitializeComponent() => throw new NotImplementedException();

        private void PermitirAcesso()
        {
            if (UserSession.Logado.Remover == false)
            {
                ColumnR.Visible = false;
            }
            if (UserSession.Logado.Alterar == false)
            {
                ColumnA.Visible = false;
            }
        }

        private void gpbBuscar_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        public void CarregarGrid()
        {
            DTO_Fornecedor dto = new DTO_Fornecedor();
            dto.Razao_Social = txtNome.Text;

            Business_Fornecedor db = new Business_Fornecedor();
            List<DTO_Fornecedor> consult = db.Consultar(dto);

            dgvCompra.AutoGenerateColumns = false;
            dgvCompra.DataSource = consult;
        }

        private void dgvCompra_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                DTO_Fornecedor linha = dgvCompra.CurrentRow.DataBoundItem as DTO_Fornecedor;
                frmAlterar_Fornecedor tela = new frmAlterar_Fornecedor();
                tela.LoadScreen(linha);

                frmMenu.Atual.OpenScreen(tela);
            }
            if (e.ColumnIndex == 5)
            {
                DTO_Fornecedor linha = dgvCompra.CurrentRow.DataBoundItem as DTO_Fornecedor;

                Business_Fornecedor db = new Business_Fornecedor();
                db.Remover(linha.ID);

                CarregarGrid();
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            CarregarGrid();
        }
    }
}
}
